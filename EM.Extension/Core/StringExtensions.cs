﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace EM.Extension.Core {

    public static class StringExtensions {

        public static string Scatter(this string self, params object[] args) {

            return String.Format(self, args);

        }

        public static string SpeakSentence(this int self) {
            var lorem = new Bogus.DataSets.Lorem(locale: "tr");
            return lorem.Sentence(self);
        }

        public static string GenerateToken(this int count) {

            var str = new StringBuilder();
            for (int i = 0; i < count; i++) {
                str.Append(Guid.NewGuid().ToString("N"));
            }
            return str.ToString();

        }

        public static string RandomDigits(this int count) {

            var random = new Bogus.Randomizer();
            var str = new StringBuilder(random.Number(1, 9).ToString());
            for (int i = 1; i < count; i++) {
                str.Append(random.Number(0, 9).ToString());
            }
            return str.ToString();

        }

        public static string GenerateMD5(this string self) {

            using (var md5 = MD5.Create()) {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(self);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++) {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }

        }

    }

}
