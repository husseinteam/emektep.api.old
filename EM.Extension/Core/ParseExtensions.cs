﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace EM.Extension.Core {
    public static class ParseExtensions {

        public static int ToInt<TItem>(this TItem self) {

            int outInt = -1;
            if (int.TryParse(self.ToString(), out outInt)) {
                return outInt;
            }
            return outInt;

        }

        public static TOut Parse<TOut>(this object self) {

            if (self.GetType().Equals(typeof(System.DBNull))) {
                return default(TOut);
            } else if (self.GetType().GetInterfaces().Any(inf => inf.Equals(typeof(IConvertible)))) {
                return (TOut)Convert.ChangeType(self, typeof(TOut));
            } else if (self.ToInt() == 0) {
                return (TOut)(object)0;
            } else {
                return (TOut)self;
            }
        }

        public static string ToJson<TItem>(this TItem item) {

            return JsonConvert.SerializeObject(item);

        }

        public static TItem FromJson<TItem>(this string self) {

            return JsonConvert.DeserializeObject<TItem>(self);

        }

        public static T FromJToken<T>(this JToken jToken, string key) {

            dynamic ret = jToken[key];
            if (ret == null) return default(T);
            if (ret is JObject) return JsonConvert.DeserializeObject<T>(ret.ToString());
            return (T)ret;

        }

    }
}
