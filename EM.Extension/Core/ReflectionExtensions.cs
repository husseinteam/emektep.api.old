﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.ComponentModel;
namespace EM.Extension.Core {

    public static class ReflectionExtensions {

        public static IEnumerable<PropertyInfo> ResolveFields<TEntity>(this TEntity self) {

            return typeof(TEntity).GetProperties();

        }

        public static bool IsAssigned<TItem>(this PropertyInfo self, TItem item) {

            var value = self.GetValue(item);
            if (value != null) {
                if (self.PropertyType.IsEnum) {
                    return true;
                }/* else if (self.PropertyType.IsNumericType()) {
                    return value.ToInt() != 0;
                } */else if (self.PropertyType.Equals(typeof(DateTime))) {
                    DateTime dt = DateTime.MinValue;
                    DateTime.TryParse(value.ToString(), out dt);
                    return dt != DateTime.MinValue && dt != DateTime.MaxValue;
                } else {
                    return value.ToString() != "";
                }
            } else {
                return false;
            }

        }

        public static bool IsNumericType(this Type t) {
            switch (Type.GetTypeCode(t)) {
                case TypeCode.Byte:
                case TypeCode.SByte:
                case TypeCode.UInt16:
                case TypeCode.UInt32:
                case TypeCode.UInt64:
                case TypeCode.Int16:
                case TypeCode.Int32:
                case TypeCode.Int64:
                case TypeCode.Decimal:
                case TypeCode.Double:
                case TypeCode.Single:
                    return true;
                default:
                    return false;
            }
        }

        public static bool IsConvertible(this Type self) {

            return self.GetInterfaces().Any(inf => inf.Equals(typeof(IConvertible)));

        }

        public static string Describe<T>(this T value) {
            if (typeof(T).IsEnum) {
                var instance = Activator.CreateInstance<T>();
                return typeof(T).GetFields().Single(fi => fi.GetValue(value).Equals(value)).GetCustomAttribute<DescriptionAttribute>().Description;
            } else {
                throw new ArgumentException("T is not an enum");
            }
        }

        public static object ExecuteMethod(this object self, string methodName,
            params object[] arguments) {

            var type = self.GetType();
            return type.GetMethod(methodName, BindingFlags.Instance | BindingFlags.NonPublic)
                .Invoke(self, arguments.ToArray());

        }

    }

}
