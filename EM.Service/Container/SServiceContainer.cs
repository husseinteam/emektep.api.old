﻿using System.Collections.Generic;
using EM.Extension.Core;
using EM.Service.Contract;
using EM.Service.Implementation;
using Ninject;

namespace EM.Service.Container {
    public static class SServiceContainer {

        private static IKernel _Kernel = new StandardKernel();

        static SServiceContainer() {
            _Kernel.Bind<IMailService>().To<MailService>();
        }

        public static void Subscribe<TContract, TImplementation>()
                where TImplementation : TContract {
            _Kernel.Bind<TContract>().To<TImplementation>();
        }

        public static TContract Resolve<TContract>() {
            if (_Kernel.CanResolve<TContract>()) {
                return _Kernel.Get<TContract>();
            } else {
                throw new KeyNotFoundException("Contract: {0} Not Subscribed"
                    .Scatter(typeof(TContract).Name));
            }
        }

    }
}
