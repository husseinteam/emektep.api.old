﻿namespace EM.Service.Enum {

    public enum EMailType {

        ActivationMail = 1,
        RecoverMail = 2,
    }
}
