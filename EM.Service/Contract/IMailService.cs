﻿using System;
using System.Collections.Specialized;
using System.Net.Mail;
using EM.Service.Enum;

namespace EM.Service.Contract {

    public interface IMailService {
        
        void SendMail(NameValueCollection values, MailAddressCollection to,
            EMailType mailType, Action callback, Action<Exception> fallback = null);
    }

}
