﻿
using System;
using System.Text;
using DM.Domain.Data;
using DM.Domain.Objects;

namespace DM.Domain.Aggregate {

    public interface IAGViewBuilder<T> where T : AGBase<T> {

        IAGMappedViewBuilder<T> MapsTo(Action<IAGSchemaBuilder> schematizer);
        IDataTools DataTools { get; }
        IAGSchemaBuilder SchemaBuilder { get; }
        StringBuilder QueryString { get; }

    }

}