﻿
using System;
using System.Linq.Expressions;
using DM.Domain.Objects;
using EM.Extension.Core;

namespace DM.Domain.Aggregate {

    public class JoinBuilder<TEntity, T> : IJoinBuilder<TEntity, T>
        where TEntity : DOBase<TEntity>
        where T : AGBase<T> {

        private readonly bool notInnerJoin;
        private readonly IAGSelectedViewBuilder<T> viewBuilder;

        public JoinBuilder(IAGSelectedViewBuilder<T> viewBuilder, bool notInnerJoin = false) {
            this.notInnerJoin = notInnerJoin;
            this.viewBuilder = viewBuilder;
        }

        public IAGSelectedViewBuilder<T> On<TOther>(Expression<Func<TEntity, TOther, bool>> selector) where TOther : DOBase<TOther> {

            var entity = Activator.CreateInstance<TEntity>() as IDOBase;
            var comparer = selector.Body as BinaryExpression;
            var leftEntity = Activator.CreateInstance(selector.Parameters[0].Type) as IDOBase;
            var rightEntity = Activator.CreateInstance(selector.Parameters[1].Type) as IDOBase;
            var left = comparer.Left.ExposeType().Equals(selector.Parameters[0].Type) ? comparer.Left : comparer.Right;
            var right = comparer.Right.ExposeType().Equals(selector.Parameters[1].Type) ? comparer.Right : comparer.Left;
            (this.viewBuilder as AGViewBuilder<T>).AppendJoinString("{0} JOIN {1} ON {2}"
                .Scatter(
                notInnerJoin ? "LEFT OUTER" : "INNER",
                entity.SchemaBuilder.GetFormatted(),
                "{0}.{1}{4}{2}.{3}".Scatter(
                    leftEntity.SchemaBuilder.GetFormatted(), left.ExposeMember().Name,
                    rightEntity.SchemaBuilder.GetFormatted(), right.ExposeMember().Name,
                    comparer.NodeTypeString()
                ))
            );
            return this.viewBuilder;

        }
    }

}