﻿
using System;
using System.Linq.Expressions;
using DM.Domain.Objects;

namespace DM.Domain.Aggregate {

    public interface IJoinBuilder<TEntity, T> 
        where TEntity : DOBase<TEntity>
        where T : AGBase<T> {
        
        IAGSelectedViewBuilder<T> On<TOther>(Expression<Func<TEntity, TOther, bool>> selector)
            where TOther : DOBase<TOther>;

    }

}