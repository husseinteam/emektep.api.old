﻿
using System;
using System.Text;
using DM.Domain.Container;
using DM.Domain.Data;
using DM.Domain.Objects;
using EM.Extension.Core;
using Ninject;

namespace DM.Domain.Aggregate {

    public class AGViewBuilder<T> : IAGViewBuilder<T>, IAGMappedViewBuilder<T>, IAGSelectedViewBuilder<T>
        where T : AGBase<T> {

        public AGViewBuilder() {
            DataTools = SKernelHost.Kernel.Get<IDataTools>();
            SchemaBuilder = new AGSchemaBuilder();
            QueryString = new StringBuilder("SELECT #selectors# FROM #table# #joinlist#");
        }

        internal void AppendJoinString(string join) {
            QueryString.Replace("#joinlist#", "{0} #joinlist#".Scatter(join));
        }

        public IAGSchemaBuilder SchemaBuilder { get; private set; }
        public IDataTools DataTools { get; private set; }
        public StringBuilder QueryString { get; private set; }

        public IAGMappedViewBuilder<T> MapsTo(Action<IAGSchemaBuilder> schematizer) {

            schematizer(this.SchemaBuilder);
            return this;

        }

        public IAGSelectedViewBuilder<T> Select<TEntity>(Action<IAGSelectList<TEntity, T>> selectCursor = null)
            where TEntity : DOBase<TEntity> {

            var entity = Activator.CreateInstance<TEntity>();
            if (selectCursor != null) {
                IAGSelectList<TEntity, T> selectList = new AGSelectList<TEntity, T>();
                selectCursor(selectList);
                QueryString.Replace("#selectors#", "{0}, #selectors#".Scatter(selectList));
            }
            QueryString.Replace("#table#", "{0}".Scatter(entity.SchemaBuilder.GetFormatted()));
            return this;

        }

        public IJoinBuilder<TEntity, T> InnerJoin<TEntity>(Action<IAGSelectList<TEntity, T>> selectCursor = null)
            where TEntity : DOBase<TEntity> {

            if (selectCursor != null) {
                var entity = Activator.CreateInstance<TEntity>() as IDOBase;
                IAGSelectList<TEntity, T> selectList = new AGSelectList<TEntity, T>();
                selectCursor(selectList);
                QueryString.Replace("#selectors#", "{0}, #selectors#".Scatter(selectList));
            }
            return new JoinBuilder<TEntity, T>(this);

        }

        public IJoinBuilder<TEntity, T> OuterJoin<TEntity>(Action<IAGSelectList<TEntity, T>> selectCursor = null)
            where TEntity : DOBase<TEntity> {

            if (selectCursor != null) {
                var entity = Activator.CreateInstance<TEntity>() as IDOBase;
                IAGSelectList<TEntity, T> selectList = new AGSelectList<TEntity, T>();
                selectCursor(selectList);
                QueryString.Replace("#selectors#", "{0}, #selectors#".Scatter(selectList));
            }
            return new JoinBuilder<TEntity, T>(this, true);

        }

    }

}