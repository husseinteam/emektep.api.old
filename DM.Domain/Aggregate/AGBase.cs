﻿
using System.Text.RegularExpressions;
using DM.Domain.Objects;
using EM.Extension.Core;

namespace DM.Domain.Aggregate {

    public abstract class AGBase<TEntity> : IAGBase
        where TEntity : AGBase<TEntity> {

        protected abstract void Map(IAGViewBuilder<TEntity> builder);

        private IAGViewBuilder<TEntity> GetBuilder() {
            IAGViewBuilder<TEntity> builder = new AGViewBuilder<TEntity>();
            Map(builder);
            return builder;
        }

        public IAGSchemaBuilder GetSchemaBuilder() {

            return GetBuilder().SchemaBuilder;

        }

        public IAGBase Drop() {
            IAGViewBuilder<TEntity> builder = GetBuilder();
            using (var engine = builder.DataTools.GenerateEngine()) {
                using (var command = engine.ConnectifiedCommand(
                    "IF EXISTS(select * FROM sys.views where name = '{0}') DROP VIEW {1}"
                        .Scatter(builder.SchemaBuilder.GetViewName(),
                            builder.SchemaBuilder.GetFormatted()))) {
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                }
            }
            return this;

        }

        public void BuildView() {
            IAGViewBuilder<TEntity> builder = GetBuilder();
            var viewAndSchema = builder.SchemaBuilder.Build();
            var selectStatement = builder.QueryString
                .Replace(", #selectors#", "")
                .Replace(" #joinlist#", "");
            var countStatement = Regex.Replace(selectStatement.ToString(), @"(SELECT )(.*)( FROM.*)", "$1COUNT(*)$3");
            var createViewStatement = selectStatement.Insert(selectStatement.ToString().ToUpper().IndexOf("SELECT") + 7, "TOP ({0})".Scatter(countStatement));
            using (var engine = builder.DataTools.GenerateEngine()) {
                using (var command = engine.ConnectifiedCommand(@"CREATE VIEW {0} WITH SCHEMABINDING AS {1}".Scatter(viewAndSchema, createViewStatement))) {
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                }
            }
        }

        public string Slug {
            get {
                IAGViewBuilder<TEntity> builder = GetBuilder();
                return builder.SchemaBuilder.GetViewName().ToLower();
            }
        }

    }

}