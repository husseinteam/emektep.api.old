﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using DM.Domain.Container;
using DM.Domain.Data;
using EM.Extension.Core;
using Ninject;

namespace DM.Domain.Aggregate {

    internal class Aggragator<T> : IAggragator<T>, ISelectedAggragator<T>, IWheredAggragator<T>, IConfinedAggragator<T>
        where T : AGBase<T> {

        private List<Expression> _SelectList = new List<Expression>();
        private Expression _DistinctColumn;
        private Queue<KeyValuePair<ExpressionType, Expression>> _WhereQueue =
            new Queue<KeyValuePair<ExpressionType, Expression>>();

        public Aggragator() {

            DataTools = SKernelHost.Kernel.Get<IDataTools>();

        }

        private IDataTools DataTools;

        public ISelectedAggragator<T> SelectColumns<TProp>(
             Expression<Func<T, TProp>> column) {

            _SelectList.Add(column.Body);
            return this;

        }

        public ISelectedAggragator<T> SelectAllColumns() {

            _SelectList.Clear();
            return this;

        }

        public IAggragator<T> SetDistinctColumn<TProp>(Expression<Func<T, TProp>> column) {

            _DistinctColumn = column.Body;
            return this;

        }

        public IWheredAggragator<T> Where(Expression<Func<T, bool>> selector) {

            _WhereQueue.Enqueue(new KeyValuePair<ExpressionType, Expression>(ExpressionType.And, selector.Body));
            return this;

        }

        public IConfinedAggragator<T> AndAlso(Expression<Func<T, bool>> selector) {

            _WhereQueue.Enqueue(new KeyValuePair<ExpressionType, Expression>(ExpressionType.AndAlso, selector.Body));
            return this;

        }

        public IConfinedAggragator<T> OrElse(Expression<Func<T, bool>> selector) {

            _WhereQueue.Enqueue(new KeyValuePair<ExpressionType, Expression>(ExpressionType.OrElse, selector.Body));
            return this;

        }

        public void ExecuteList(Action<T> cursor, Action lastback = null) {

            var commandText = this.ToString();
            using (var engine = DataTools.GenerateEngine()) {
                using (var command = engine.ConnectifiedCommand(commandText)) {
                    using (var reader = command.ExecuteReader()) {
                        while (reader.Read()) {
                            var et = ExtractFrom(reader);
                            cursor(et);
                        }
                    }
                    command.Connection.Close();
                }
            }
            if (lastback != null) {
                lastback();
            }

        }

        public void ExecuteSingle(Action<T> cursor) {

            var commandText = this.ToString();
            using (var engine = DataTools.GenerateEngine()) {
                using (var command = engine.ConnectifiedCommand(commandText)) {
                    using (var reader = command.ExecuteReader()) {
                        if (reader.Read()) {
                            var et = ExtractFrom(reader);
                            cursor(et);
                        }
                    }
                    command.Connection.Close();
                }
            }

        }

        public override string ToString() {

            var entitySchemaBuilder =
                (Activator.CreateInstance(typeof(T)) as IAGBase).GetSchemaBuilder();
            var selectList = "{0}.*".Scatter(entitySchemaBuilder.GetFormatted());
            var viewAndSchema = entitySchemaBuilder.GetFormatted();
            if (_SelectList.Count > 0) {
                if (_DistinctColumn == null) {
                    selectList = _SelectList.Select(se => "{0}.[{1}]".Scatter(
                        viewAndSchema, se.ExposeMember().Name))
                        .Aggregate((p, n) => p + ", " + n);
                } else {
                    selectList = _SelectList.Where(se => se.ExposeMember().Name != _DistinctColumn.ExposeMember().Name)
                        .Select(se => "{0}.[{1}]".Scatter(viewAndSchema,
                            se.ExposeMember().Name))
                        .Aggregate((p, n) => p + ", " + n);
                    selectList.Insert(0, "DISTINCT {0}.[{1}], ".Scatter(viewAndSchema,
                            _DistinctColumn.ExposeMember().Name));
                }
            } else {
                if (_DistinctColumn != null) {
                    selectList = selectList.Insert(0, "DISTINCT {0}.[{1}], ".Scatter(viewAndSchema, _DistinctColumn.ExposeMember().Name));
                }
            }
            var str = new StringBuilder("SELECT {0} FROM {1} WHERE 1=1".Scatter(selectList, viewAndSchema));

            Action<Expression, StringBuilder> reducer = null;
            reducer = (be, sb) => {
                var bem = be as BinaryExpression;
                if (bem != null) {
                    if (bem.Left is BinaryExpression) {
                        reducer(bem.Left as BinaryExpression, sb);
                        str.AppendFormat(" {0} ", bem.NodeTypeString() == "&&" ? "AND" : "OR");
                    } else if (bem.Left is UnaryExpression) {
                        str.AppendFormat("{0}.[{1}]", entitySchemaBuilder.GetFormatted(),
                            bem.Left.ExposeMember().Name);
                        str.AppendFormat(" {0} ", bem.NodeTypeString());
                    } else if (bem.Left is MemberExpression) {
                        str.AppendFormat("{0}.[{1}]", entitySchemaBuilder.GetFormatted(),
                            bem.Left.ExposeMember().Name);
                        str.AppendFormat(" {0} ", bem.NodeTypeString());
                    }
                    if (bem.Right is BinaryExpression) {
                        reducer(bem.Right as BinaryExpression, sb);
                    } else {
                        str.AppendFormat("{0}", bem.Right.Resultify());
                    }
                }
            };

            while (_WhereQueue.Count > 0) {
                var toq = _WhereQueue.Dequeue();
                var operand = "AND";
                if (toq.Key == ExpressionType.OrElse) {
                    operand = "OR";
                }
                str.AppendFormat(" {0} ", operand);
                reducer(toq.Value, str);
            }
            return str.ToString();

        }

        private static T ExtractFrom(System.Data.IDataReader reader) {
            var et = Activator.CreateInstance<T>() as T;
            foreach (var p in typeof(T).GetProperties()) {
                if (reader.HasColumn(p.Name)) {
                    var value = reader[p.Name];
                    if (p.PropertyType.IsEnum) {
                        p.SetValue(et, Enum.Parse(p.PropertyType, value.ToString()));
                    } else if (p.PropertyType.IsConvertible()) {
                        p.SetValue(et, Convert.ChangeType(value, p.PropertyType));
                    } else {
                        p.SetValue(et, reader[p.Name]);
                    }
                }
            }

            return et;
        }

    }

}