﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using DM.Domain.Container;
using DM.Domain.Data;
using EM.Extension.Core;
using Ninject;

namespace DM.Domain.Objects {
    internal class DOUniqueKeyBuilder : ILineBuilder {

        private IEnumerable<PropertyInfo> uniqueKeyProps;
        private IDOSchemaBuilder hostSchemaBuilder;

        public DOUniqueKeyBuilder(IDOSchemaBuilder hostSchemaBuilder, IEnumerable<PropertyInfo> uniqueKeyProps) {
            this.hostSchemaBuilder = hostSchemaBuilder;
            this.uniqueKeyProps = uniqueKeyProps;
        }

        public string Build() {

            switch (SKernelHost.Kernel.Get<IDataTools>().CurrentServerType) {
                case EServerType.MySQL:
                    throw new NotImplementedException("MySql isn't Supported");
                case EServerType.MSSql:
                    return "CONSTRAINT UQ_{0}_{1} UNIQUE ({2})".Scatter(
                        this.hostSchemaBuilder.GetTableName(),
                        this.uniqueKeyProps.Select(p => p.Name).Aggregate((prev, next) => 
                            "{0}_{1}".Scatter(prev, next)),
                        this.uniqueKeyProps.Select(p => p.Name).Aggregate((prev, next) => "{0}, {1}".Scatter(prev, next))
                    );
                default:
                    throw new NotImplementedException("Unsupported option");
            }

        }
    }
}