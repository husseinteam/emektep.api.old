﻿
using System;
using DM.Domain.Container;
using DM.Domain.Data;
using EM.Extension.Core;
using Ninject;

namespace DM.Domain.Objects {

    public class DOSchemaBuilder : IDOSchemaBuilder {

        public string schemaName { get; private set; }
        public string tableName { get; private set; }

        public string FormatsBy(string format) {
            return format.Scatter(this.schemaName, this.tableName);
        }

        public IDOSchemaBuilder SchemaName(string schemaName) {

            this.schemaName = schemaName;
            return this;

        }

        public IDOSchemaBuilder TableName(string tableName) {

            this.tableName = tableName;
            return this;

        }
        public string GetTableName() {
            return this.tableName;
        }

        public string GetFormatted() {
            switch (SKernelHost.Kernel.Get<IDataTools>().CurrentServerType) {
                case EServerType.MySQL:
                    throw new NotImplementedException("MySql isn't Supported");
                case EServerType.MSSql:
                    return "[{0}].[{1}]".Scatter(this.schemaName, this.tableName);
                default:
                    throw new NotImplementedException("Unsupported keyType");
            }
        }

        public string Build() {

            switch (SKernelHost.Kernel.Get<IDataTools>().CurrentServerType) {
                case EServerType.MySQL:
                    throw new NotImplementedException("MySql isn't Supported");
                case EServerType.MSSql:
                    var script = @"
IF NOT EXISTS (SELECT schema_name 
    FROM INFORMATION_SCHEMA.SCHEMATA 
    WHERE schema_name = '{0}' )
BEGIN
    EXEC sp_executesql N'CREATE SCHEMA {0};';
END";
                    using (var command = SKernelHost.Kernel.Get<IDataTools>().Engine.ConnectifiedCommand(script.Scatter(schemaName))) {
                        command.ExecuteNonQuery();
                        command.Connection.Close();
                    }
                    return GetFormatted();
                default:
                    throw new NotImplementedException("Unsupported keyType");
            }

        }

    }

}