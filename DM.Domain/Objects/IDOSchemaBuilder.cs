﻿
namespace DM.Domain.Objects {

    public interface IDOSchemaBuilder : ILineBuilder, IFormatter {

        IDOSchemaBuilder SchemaName(string schemaName);
        IDOSchemaBuilder TableName(string tableName);
        string GetTableName();

    }

}