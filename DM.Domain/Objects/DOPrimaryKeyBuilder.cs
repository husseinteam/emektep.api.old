﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using DM.Domain.Container;
using DM.Domain.Data;
using EM.Extension.Core;
using Ninject;

namespace DM.Domain.Objects {
    internal class DOPrimaryKeyBuilder : ILineBuilder {

        private IEnumerable<PropertyInfo> primaryKeyProps;
        private IDOSchemaBuilder hostSchemaBuilder;

        public IEnumerable<PropertyInfo> PrimaryKeyProps {
            get {
                return primaryKeyProps;
            }
        }

        public DOPrimaryKeyBuilder(IDOSchemaBuilder hostSchemaBuilder, IEnumerable<PropertyInfo> primaryKeyProps) {

            this.hostSchemaBuilder = hostSchemaBuilder;
            this.primaryKeyProps = primaryKeyProps;

        }

        public string Build() {

            switch (SKernelHost.Kernel.Get<IDataTools>().CurrentServerType) {
                case EServerType.MySQL:
                    throw new NotImplementedException("MySql isn't Supported");
                case EServerType.MSSql:
                    return "CONSTRAINT PK_{0} PRIMARY KEY ({1})".Scatter(
                        this.hostSchemaBuilder.GetTableName(),
                        this.PrimaryKeyProps.Select(p => p.Name).Aggregate((prev, next) => "{0}, {1}".Scatter(prev, next))
                    );
                default:
                    throw new NotImplementedException("Unsupported option");
            }

        }
    }
}