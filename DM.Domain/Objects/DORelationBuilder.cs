﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using DM.Domain.Container;
using DM.Domain.Data;
using EM.Extension.Core;
using Ninject;

namespace DM.Domain.Objects {

    public class DORelationBuilder : ILineBuilder {

        private IEnumerable<PropertyInfo> piForeignKeys;
        private IEnumerable<PropertyInfo> piReferencedKeys;
        private IDOSchemaBuilder hostSchemaBuilder;
        private IDOSchemaBuilder referencedSchemaBuilder;

        public DORelationBuilder(IDOSchemaBuilder hostSchemaBuilder, IEnumerable<PropertyInfo> piForeignKeys) {

            this.hostSchemaBuilder = hostSchemaBuilder;
            this.piForeignKeys = piForeignKeys;

        }

        public void References<TDomain>(params Expression<Func<TDomain, int>>[] selectors)
            where TDomain : DOBase<TDomain> {

            var domain = Activator.CreateInstance<TDomain>();
            //domain.TableBuilder.Build();
            this.referencedSchemaBuilder = domain.SchemaBuilder;
            this.piReferencedKeys = selectors.Select(s => s.ResolveMember());

        }

        public string Build() {
            switch (SKernelHost.Kernel.Get<IDataTools>().CurrentServerType) {
                case EServerType.MySQL:
                    throw new NotImplementedException("MySql isn't Supported");
                case EServerType.MSSql:
                    return "CONSTRAINT FK_{0}_{1} FOREIGN KEY ({2}) REFERENCES {3}({4}) ON DELETE NO ACTION ON UPDATE NO ACTION".Scatter(
                        this.hostSchemaBuilder.GetTableName(),
                        this.referencedSchemaBuilder.GetTableName(),
                        this.piForeignKeys.Select(p => p.Name).Aggregate((prev, next) => "{0},{1}".Scatter(prev, next)),
                        this.referencedSchemaBuilder.GetFormatted(),
                        this.piReferencedKeys.Select(p => p.Name).Aggregate((prev, next) => "{0},{1}".Scatter(prev, next))
                        );
                default:
                    throw new NotImplementedException("Unsupported option");
            }
        }

    }

}