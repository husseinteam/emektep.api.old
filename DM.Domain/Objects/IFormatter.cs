﻿namespace DM.Domain.Objects {
    public interface IFormatter : ILineBuilder {

        string GetFormatted();

    }
}
