﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using DM.Domain.Container;
using DM.Domain.Data;
using DM.Domain.Extensions;
using EM.Extension.Core;
using Ninject;

namespace DM.Domain.Objects {

	internal class DOTableBuilder<TEntity> : DOConstraintBuilder<TEntity>, IDOTableBuilder<TEntity>
		where TEntity : DOBase<TEntity> {

		public IDataTools DataTools { get; private set; }

        public DOTableBuilder(IDataTools dataTools) {
            this.DataTools = dataTools;
        }

		public IDOSchemaBuilder SchemaBuilder {
			get {
				return schemaBuilder;
			}
		}
        
		private List<PropertyInfo> requiredFields = new List<PropertyInfo>();
		private List<ILineBuilder> uniqueKeyBuilders = new List<ILineBuilder>();
		private List<ILineBuilder> primaryKeyBuilders = new List<ILineBuilder>();

		public IEnumerable<PropertyInfo> Fields {
			get {
				return fields;
			}
		}

		public IEnumerable<PropertyInfo> RequiredFields() {
			return propBuilders.Where(pb => (pb as DOPropBuilder).isRequired).Select(pb => (pb as DOPropBuilder).prop).ToArray();
		}

        public IEnumerable<DOPropBuilder> AutoGeneratedBuilders() {
            return propBuilders.Where(pb => (pb as DOPropBuilder).AutoGenerated).Select(pb => (pb as DOPropBuilder)).ToArray();
        }

        public IEnumerable<PropertyInfo> PKFields() {
			var li = new List<PropertyInfo>();
			foreach (var pkb in primaryKeyBuilders) {
				li.AddRange((pkb as DOPrimaryKeyBuilder).PrimaryKeyProps);
			}
			return li.ToArray();
		}

		public DOPropBuilder For<TProp>(Expression<Func<TEntity, TProp>> selector) {

			var field = selector.ResolveMember();
			fields.Add(field);
			var propBuilder = new DOPropBuilder(field);
			propBuilders.Add(propBuilder);
			return propBuilder;

		}

		public void UniqueKey<TProp>(params Expression<Func<TEntity, TProp>>[] selectors) {

			selectors.Select(s => s.ResolveMember()).ToList().ForEach(s => {
				if (!fields.Contains(s)) {
					propBuilders.Add(new DOPropBuilder(s).IsTypeOf(typeof(TProp).GetDataType()).IsRequired());
					fields.Add(s);
				}
			});
			var uniqueKeyBuilder = new DOUniqueKeyBuilder(SchemaBuilder, selectors.Select(s => s.ResolveMember()));
			uniqueKeyBuilders.Add(uniqueKeyBuilder);

		}

		public void PrimaryKey<TProp>(params Expression<Func<TEntity, TProp>>[] selectors) {

			var primaryKeyBuilder = new DOPrimaryKeyBuilder(SchemaBuilder, selectors.Select(s => s.ResolveMember()));
			primaryKeyBuilders.Add(primaryKeyBuilder);

		}

		public void MapsTo(Action<IDOSchemaBuilder> schematizer) {

			schematizer(this.SchemaBuilder);

		}

		public void BuildTable() {

			var tableAndSchema = this.SchemaBuilder.Build();
			var columns = this.propBuilders.Select(pb => pb.Build()).Aggregate((prev, next) => "{0}, {1}".Scatter(prev, next));
			var script = "CREATE TABLE {0}( {1} );".Scatter(tableAndSchema, columns);
			using (var command = DataTools.Engine.ConnectifiedCommand(script)) {
				command.ExecuteNonQuery();
				if (primaryKeyBuilders.Count > 0) {
					foreach (var builder in primaryKeyBuilders) {
						var pkscript = "ALTER TABLE {0} ADD {1};".Scatter(tableAndSchema, builder.Build());
						command.CommandText = pkscript;
						command.ExecuteNonQuery();
					}
				}
				command.Connection.Close();
			}
		}

		public void BuildConstraints() {

			var tableAndSchema = this.SchemaBuilder.Build();
			var builders = relationBuilders.Union(uniqueKeyBuilders);
			using (var command = DataTools.Engine.ConnectifiedCommand()) {
				foreach (var builder in builders) {
					var script = "ALTER TABLE {0} ADD {1};".Scatter(tableAndSchema, builder.Build());
					command.CommandText = script;
					command.ExecuteNonQuery();
				}
				command.Connection.Close();
			}

		}
		public IDOTableBuilder<TEntity> CutOff() {
			var tableAndSchema = this.SchemaBuilder.Build();
			var script = @"

if exists (select * from sysobjects where name='{0}' and xtype='U')
begin
DECLARE @sql varchar(800)
DECLARE alter_cursor CURSOR
	FOR SELECT 
	'ALTER TABLE ' +  OBJECT_SCHEMA_NAME(parent_object_id) +
	'.[' + OBJECT_NAME(parent_object_id) + 
	'] DROP CONSTRAINT ' + name
FROM sys.foreign_keys
WHERE referenced_object_id = object_id('{1}')
OPEN alter_cursor
FETCH NEXT FROM alter_cursor INTO @sql
WHILE @@FETCH_STATUS = 0  
BEGIN  
	EXEC (@sql)
	FETCH NEXT FROM alter_cursor INTO @sql
END

CLOSE alter_cursor
DEALLOCATE alter_cursor

DROP TABLE {1}
end
".Scatter(this.SchemaBuilder.GetTableName(), tableAndSchema);
			using (var command = DataTools.Engine.ConnectifiedCommand(script)) {
				command.ExecuteNonQuery();
				command.Connection.Close();
			}
			return this;

		}
	}

}