﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using DM.Domain.Aggregate;
using DM.Domain.Objects;

namespace DM.Domain.Data {
    public static class SBuildEngine {

        public static void BuildDomain(AssemblyName assemblyName) {

            var assembly = Assembly.Load(assemblyName);
            var domainObjects = new List<IDOBase>();
            var aggregateObjects = new List<IAGBase>();

            foreach (var t in assembly.GetTypes()) {
                try {
                    if (t.IsSubclassOf(typeof(DOBase<>).MakeGenericType(t))) {
                        domainObjects.Add(Activator.CreateInstance(t) as IDOBase);
                    }
                } catch {
                    continue;
                }
            }

            foreach (var t in assembly.GetTypes()) {
                try {
                    if (t.IsSubclassOf(typeof(AGBase<>).MakeGenericType(t))) {
                        aggregateObjects.Add(Activator.CreateInstance(t) as IAGBase);
                    }
                } catch {
                    continue;
                }
            }

            aggregateObjects.ForEach(dobj => dobj.Drop());
            domainObjects.ForEach(dobj => dobj.Drop());

            domainObjects.ForEach(dobj => dobj.BuildTable());
            domainObjects.ForEach(dobj => dobj.BuildConstraints());
            aggregateObjects.ForEach(dobj => dobj.BuildView());

        }

        public static Type BuildType(AssemblyName assemblyName, string slug) {

            var assembly = Assembly.Load(assemblyName);
            foreach (var t in assembly.GetTypes()) {
                try {
                    if (t.GetInterfaces().Any(inf => inf.Equals(typeof(IDOBase)))) {
                        if ((Activator.CreateInstance(t) as IDOBase).Slug == slug) {
                            return t;
                        }
                    } else if (t.GetInterfaces().Any(inf => inf.Equals(typeof(IAGBase)))) {
                        if ((Activator.CreateInstance(t) as IAGBase).Slug == slug) {
                            return t;
                        }
                    }
                } catch {
                    continue;
                }
            }
            return null;

        }

    }
}
