﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using EM.Extension.Core;

namespace DM.Domain.Data {

    internal class DataTools : IDataTools, IConnectionCredentials {

        public string ConnectionString() {

            switch (CurrentServerType) {
                case EServerType.MSSql:
#if DEBUG
                    return "Data Source=.;Initial Catalog=emdb;Persist Security Info=True;User ID=emadmin; Password=1q2w3e4r5t";
#else
                    return "Server=37.230.108.244,1433;Database=llfdb;User ID=llfadmin;Password=YrQ3Zj0z;";
#endif
                case EServerType.MySQL:
                    throw new NotImplementedException("MySql isn't Supported");
                default:
                    throw new NotImplementedException("Unsupported option");
            }

        }


        public EServerType CurrentServerType {
            get {
                return EServerType.MSSql;
            }
        }

        public string SqlExpression(ExpressionType nodeType) {

            switch (CurrentServerType) {
                case EServerType.MySQL:
                    throw new NotImplementedException("MySql isn't Supported");
                case EServerType.MSSql:
                    switch (nodeType) {
                        case ExpressionType.Equal: return "=";
                        case ExpressionType.NotEqual: return "!=";
                        case ExpressionType.GreaterThanOrEqual: return ">=";
                        case ExpressionType.GreaterThan: return ">";
                        case ExpressionType.LessThanOrEqual: return "<=";
                        case ExpressionType.LessThan: return "<";
                        default:
                            throw new NotImplementedException("Unsupported option: {0}".Scatter(nodeType));
                    }
                default:
                    throw new NotImplementedException("Unsupported option");
            }

        }

        private DataEngine _DataEngine;

        public IDataEngine Engine {

            get { return (_DataEngine = _DataEngine ?? new DataEngine(CurrentServerType, ConnectionString())); }

        }

        public IDataEngine GenerateEngine() {
            return new DataEngine(CurrentServerType, ConnectionString());
        }

        public string ParseFields(IEnumerable<PropertyInfo> fields) {
            switch (CurrentServerType) {
                case EServerType.MSSql:
                    return fields.Select(pr => pr.Name).Aggregate((prev, next) => prev + ", " + next);
                case EServerType.MySQL:
                    throw new NotImplementedException("MySql isn't Supported");
                default:
                    throw new NotImplementedException("Unsupported option");
            }
        }

        public string ParseParams(IEnumerable<PropertyInfo> fields) {
            switch (CurrentServerType) {
                case EServerType.MSSql:
                    return fields.Select(pr => "@" + pr.Name).Aggregate((prev, next) => prev + ", " + next);
                case EServerType.MySQL:
                    throw new NotImplementedException("MySql isn't Supported");
                default:
                    throw new NotImplementedException("Unsupported option");
            }
        }

    }

}
