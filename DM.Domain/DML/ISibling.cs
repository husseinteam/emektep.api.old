﻿using System;
using DM.Domain.Objects;

namespace DM.Domain.DML {

    public interface ISibling<TEntity>
        where TEntity : DOBase<TEntity> {

        ISibling<TEntity> Next<TOther>(Func<IExecutedResult<TEntity>, TOther> cursor) where TOther : DOBase<TOther>;
        TEntity Execute(Action<IExecutedResultList> cursor);

    }

}