﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DM.Domain.Container;
using DM.Domain.Data;
using DM.Domain.Enums;
using DM.Domain.Objects;
using Ninject;

namespace DM.Domain.DML {

    class Sibling<TEntity> : ISibling<TEntity>
        where TEntity : DOBase<TEntity> {

        protected readonly TEntity entity;
        private List<KeyValuePair<Type, object>> chainedEntities = new List<KeyValuePair<Type, object>>();

        private IDataTools DataTools { get; set; }

        public Sibling(TEntity entity) {
            this.entity = entity;
            this.DataTools = SKernelHost.Kernel.Get<IDataTools>();
        }

        public ISibling<TEntity> Next<TOther>(Func<IExecutedResult<TEntity>, TOther> cursor)
            where TOther : DOBase<TOther> {

            var other = cursor(new ExecutedResult<TEntity>(entity));
            chainedEntities.Add(new KeyValuePair<Type, object>(typeof(TOther), other));
            return this;

        }

        public TEntity Execute(Action<IExecutedResultList> cursor) {

            var celi = new List<object>();
            foreach (var chained in chainedEntities) {
                IDOBase ce = (IDOBase)Convert.ChangeType(chained.Value, chained.Key);
                var insertClause = new StringBuilder("INSERT INTO #table#(#fields#) VALUES(#values#)");
                var fields = ce.TakeFields(EResolveBy.AllAssigned).Where(f => f.Name != "ID");
                insertClause
                    .Replace("#table#", ce.SchemaBuilder.GetFormatted())
                    .Replace("#fields#", DataTools.ParseFields(fields))
                    .Replace("#values#", DataTools.ParseParams(fields));

                using (var engine = DataTools.GenerateEngine()) {
                    using (var command = engine.ConnectifiedCommand(insertClause.ToString())) {
                        foreach (var field in fields) {
                            if (field.PropertyType.IsEnum) {
                                engine.AddWithValue(field.Name, (int)field.GetValue(ce));
                            } else if (field.PropertyType.GetInterfaces().Any(inf => inf.Equals(typeof(IConvertible)))) {
                                engine.AddWithValue(field.Name, Convert.ChangeType(field.GetValue(ce), field.PropertyType));
                            } else {
                                engine.AddWithValue(field.Name, field.GetValue(ce));
                            }
                        }
                        var rowc = command.ExecuteNonQuery();
                        if (rowc == 1) {
                            using (var identityCommand = engine.IdentityCommand(command)) {
                                ce.ID = Convert.ToInt32(identityCommand.ExecuteScalar());
                                identityCommand.Connection.Close();
                            }
                            celi.Add(ce);
                        } else {
                            command.Connection.Close();
                            return entity;
                        }
                    }
                }
            }
            cursor(new ExecutedResultList(celi.ToArray()));
            return entity;

        }

    }

}