﻿using System;
using System.Linq.Expressions;
using DM.Domain.Objects;

namespace DM.Domain.Query {

    public interface IOrderedList<TEntity> : ICursorExecuter<TEntity>
        where TEntity : DOBase<TEntity> {

        IOrderedList<TEntity> By<TProp>(Expression<Func<TEntity, TProp>> selector);

    }

}
