﻿
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using DM.Domain.Container;
using DM.Domain.Data;
using DM.Domain.Enums;
using DM.Domain.Objects;
using EM.Extension.Core;
using Ninject;

namespace DM.Domain.Query {

    class QueryBuilder : IQueryBuilder {

        private StringBuilder _QueryString;
        private StringBuilder _FieldsString;
        private StringBuilder _ConstraintString;
        private StringBuilder _OrderByString;

        public IDataTools DataTools { get; private set; }
        public IDOSchemaBuilder SchemaBuilder { get; set; }

        private string FieldNameFromProperty(PropertyInfo prop) {

            return "{0}.[{1}], ".Scatter(this.SchemaBuilder.GetFormatted(), prop.Name);

        }

        public List<PropertyInfo> Fields { get; private set; }

        public QueryBuilder(IDOSchemaBuilder schemaBuilder) {

            _QueryString = new StringBuilder("SELECT #fields# FROM #table# #where# #orderby#");
            _FieldsString = new StringBuilder("*");
            _ConstraintString = new StringBuilder("WHERE 1=1 ");
            _OrderByString = new StringBuilder("ORDER BY ");
            Fields = new List<PropertyInfo>();
            this.SchemaBuilder = schemaBuilder;
            this.DataTools = SKernelHost.Kernel.Get<IDataTools>();

        }

        public IQueryBuilder AddField(PropertyInfo prop) {

            if (_FieldsString.ToString().Equals("*")) {
                _FieldsString.Clear();
                Fields.Clear();
            }
            if (!Fields.Contains(prop)) {
                _FieldsString.Append(FieldNameFromProperty(prop));
                Fields.Add(prop);
            }
            return this;

        }

        public IQueryBuilder RemoveField<TEntity>(PropertyInfo prop)
            where TEntity : DOBase<TEntity> {

            if (Fields.Count == 0) {
                Fields.AddRange(Activator.CreateInstance<TEntity>().TakeFields(EResolveBy.AllAssigned));
            }
            if (Fields.Contains(prop)) {
                _FieldsString.Replace(FieldNameFromProperty(prop), "");
                Fields.Remove(prop);
            }
            if (_FieldsString.ToString().Equals("")) {
                _FieldsString.Append("*");
            }
            return this;

        }

        public IQueryBuilder AndConstraint(PropertyInfo prop, object value, ExpressionType nodeType) {

            _ConstraintString.AppendFormat(" AND {0}{2}@{1}", "{0}.[{1}]".Scatter(this.SchemaBuilder.GetFormatted(), prop.Name), prop.Name, DataTools.SqlExpression(nodeType));
            DataTools.Engine.AddWithValue("@{0}".Scatter(prop.Name), value);
            return this;

        }

        public IQueryBuilder OrConstraint(PropertyInfo prop, object value, ExpressionType nodeType) {

            _ConstraintString.AppendFormat(" OR {0}{2}@{1}", "{0}.[{1}]".Scatter(this.SchemaBuilder.GetFormatted(), prop.Name), prop.Name, DataTools.SqlExpression(nodeType));
            DataTools.Engine.AddWithValue("@{0}".Scatter(prop.Name), value);
            return this;

        }

        public IQueryBuilder OrderByField(PropertyInfo prop) {

            _OrderByString.AppendFormat("{0}, ", FieldNameFromProperty(prop));
            return this;

        }

        public StringBuilder BuildQuery() {

            _QueryString.Replace("#fields#", _FieldsString.ToString().TrimEnd(' ', ','))
                .Replace("#table#", this.SchemaBuilder.GetFormatted())
                .Replace("#where#", _ConstraintString.ToString() == "WHERE 1=1 " ? "" : _ConstraintString.ToString().TrimEnd(' ', ','))
                .Replace("#orderby#", _OrderByString.ToString() == "ORDER BY " ? "" : _OrderByString.ToString().TrimEnd(' ', ','));
            return _QueryString;
        }
    }

}