﻿using System;
using System.Linq.Expressions;
using DM.Domain.Objects;

namespace DM.Domain.Query {

    public interface ISelectList<TEntity> : ICursorExecuter<TEntity>
        where TEntity : DOBase<TEntity> {

        bool AllColumns { get; }
        ISelectList<TEntity> Add<TProp>(Expression<Func<TEntity, TProp>> columnSelector);
        ISelectList<TEntity> Remove<TProp>(Expression<Func<TEntity, TProp>> columnSelector);
        IConfinedList<TEntity> Where(Expression<Func<TEntity, bool>> columnSelector);
        IOrderedList<TEntity> Order();

    }

}