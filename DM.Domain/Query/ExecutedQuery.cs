﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using DM.Domain.Container;
using DM.Domain.Data;
using DM.Domain.Enums;
using DM.Domain.Objects;
using EM.Extension.Core;
using Ninject;

namespace DM.Domain.Query {

    public class ExecutedQuery<TEntity> : IExecutedQuery<TEntity>
        where TEntity : DOBase<TEntity> {

        private List<PropertyInfo> fields = new List<PropertyInfo>();
        private IDataReader reader;
        private TEntity entity;
        private readonly List<KeyValuePair<PropertyInfo, object>> _UpdateParameters = new List<KeyValuePair<PropertyInfo, object>>();
        public IDataTools DataTools { get; private set; }

        public ExecutedQuery(IDataReader reader, IList<PropertyInfo> fields, int index) {
            this.reader = reader;
            this.fields = fields.Count > 0 ? new List<PropertyInfo>(fields) : new List<PropertyInfo>(Activator.CreateInstance<TEntity>().TakeFields(EResolveBy.AllAssigned));
            this.Index = Index;
            this.DataTools = SKernelHost.Kernel.Get<IDataTools>();
            ParseEntity(this.fields.ToArray());
        }

        public TEntity ResolvedEntity {
            get {
                return entity;
            }
        }

        public int Index { get; private set; }

        internal List<KeyValuePair<PropertyInfo, object>> UpdateParameters {
            get {
                return _UpdateParameters;
            }
        }

        private void ParseEntity(IEnumerable<PropertyInfo> entityFields) {

            entity = Activator.CreateInstance<TEntity>();
            foreach (var ef in entityFields) {
                if (this.reader.HasColumn(ef.Name)) {
                    var value = this.reader[ef.Name];
                    if (value == null || value == DBNull.Value) {

                    } else if (ef.PropertyType.IsEnum) {
                        ef.SetValue(entity, Convert.ChangeType(Enum.ToObject(ef.PropertyType, value), ef.PropertyType));
                    } else if (ef.PropertyType.GetInterfaces().Any(inf => inf.Equals(typeof(IConvertible)))) {
                        ef.SetValue(entity, Convert.ChangeType(value, ef.PropertyType));
                    } else {
                        ef.SetValue(entity, value);
                    } 
                }
            }

        }

        public IUpdateClause<TEntity> Modify() {

            return new UpdateClause<TEntity>(this);

        }

        internal void AddUpdateParameter<TProp>(PropertyInfo prop, TProp value) {
            UpdateParameters.Add(new KeyValuePair<PropertyInfo, object>(prop, prop.GetValue(ResolvedEntity)));
        }

        public bool Delete(Action<Exception> fallback = null) {
            var deleteClause = new StringBuilder("DELETE FROM #table# WHERE #selector#");
            var pkli = (entity as IDOBase).TakeFields(EResolveBy.AllPKs);
            var selectors = pkli.Select(p => "{0}=@{0}".Scatter(p.Name))
                .Aggregate((p, n) => "{0} AND {1}".Scatter(p, n));
            deleteClause
                .Replace("#table#", (entity as IDOBase).SchemaBuilder.GetFormatted())
                .Replace("#selector#", selectors);
            using (var engine = this.DataTools.GenerateEngine()) {
                using (var command = engine.ConnectifiedCommand(deleteClause.ToString())) {
                    foreach (var id in pkli) {
                        engine.AddWithValue("@{0}".Scatter(id.Name), id.GetValue(this.entity));
                    }
                    try {
                        return command.ExecuteNonQuery() > 0;
                    } catch (Exception ex) {
                        if (fallback != null) {
                            fallback(ex);
                        }
                        return false;
                    }
                }
            }
        }
    }
}