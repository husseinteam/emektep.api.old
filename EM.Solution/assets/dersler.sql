SELECT DY.*, PR.*, OU.*, MU.*, DN.*, DS.* FROM [MF].[DersYuku] DY
INNER JOIN [KR].[Programlar] PR ON PR.ID=DY.ProgramID
INNER JOIN [AK].[OgretimUyeleri] OU ON OU.ID=DY.OgretimUyesiID
INNER JOIN [MF].[Mufredatlar] MU ON MU.ProgramID=PR.ID
INNER JOIN [MF].[Donemler] DN ON DN.MufredatID=MU.ID
INNER JOIN [MF].[Dersler] DS ON DS.DonemID=DN.ID
