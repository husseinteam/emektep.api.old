﻿namespace EM.Entities.VectorObjects {

    public class LogonCredential {

        public string Logon { get; set; }

        public string Password { get; set; }

    }

}
