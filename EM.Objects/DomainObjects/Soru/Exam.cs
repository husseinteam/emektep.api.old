﻿using System;
using DM.Domain.Objects;
using EM.Objects.DomainObjects.AkademikKadro;
using EM.Objects.DomainObjects.Core;
using EM.Objects.DomainObjects.Mufredat;

namespace EM.Objects.DomainObjects.Soru {

    public class Exam : EMBase<Exam> {

        public string Title { get; set; }

        public DateTime ExamDate { get; set; }

        public int StudentID { get; set; }

        public int LectureID { get; set; }

        protected override void EMMap(IDOTableBuilder<Exam> builder) {
            
            builder.MapsTo(x => { x.SchemaName("SV").TableName("Exams"); });
            builder.For(d => d.ExamDate).IsTypeOf(EDataType.DateTime).IsRequired();

            builder.ForeignKey(tc => tc.StudentID).References<Ogrenci>(s => s.ID);
            builder.ForeignKey(tc => tc.LectureID).References<Ders>(s => s.ID);

            builder.UniqueKey(t => t.StudentID, t => t.LectureID);

        }


    }
}
