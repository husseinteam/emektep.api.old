﻿using DM.Domain.Objects;
using EM.Objects.DomainObjects.Core;

namespace EM.Objects.DomainObjects.Mufredat {

    public class Ders : EMBase<Ders> {

        public string DersKodu { get; set; }

        public string DersAdi { get; set; }

        public int TeorikSaat { get; set; }

        public int UygulamaSaat { get; set; }

        public int Kredi { get; set; }
    
        public int DonemID { get; set; }

        protected override void EMMap(IDOTableBuilder<Ders> builder) {
            
            builder.MapsTo(x => { x.SchemaName("MF").TableName("Dersler"); });
            builder.For(d => d.DersKodu).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(32);
            builder.For(d => d.DersAdi).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(128);
            builder.For(d => d.TeorikSaat).IsTypeOf(EDataType.Int).IsRequired();
            builder.For(d => d.UygulamaSaat).IsTypeOf(EDataType.Int).IsRequired();
            builder.For(d => d.Kredi).IsTypeOf(EDataType.Int).IsRequired();

            builder.ForeignKey(x => x.DonemID).References<Donem>(x => x.ID);

        }

    }
}
