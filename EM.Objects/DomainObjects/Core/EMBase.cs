﻿using DM.Domain.Objects;

namespace EM.Objects.DomainObjects.Core {

    public abstract class EMBase<TEntity> : DOBase<TEntity>
        where TEntity : DOBase<TEntity> {

        protected sealed override void Map(IDOTableBuilder<TEntity> builder) {
            builder.For(d => d.ID).IsTypeOf(EDataType.Int).IsIdentity();
            builder.PrimaryKey(d => d.ID);
            EMMap(builder);
        }

        protected abstract void EMMap(IDOTableBuilder<TEntity> builder);

    }

}
