﻿using DM.Domain.Objects;
using EM.Entities.DomainObjects.AkademikKadro;
using EM.Entities.DomainObjects.Uyelik;
using EM.Objects.DomainObjects.Core;

namespace EM.Objects.DomainObjects.AkademikKadro {

    public class Ogrenci : EMBase<Ogrenci> {

        public string OgrenciNo { get; set; }

        public int KayitYili { get; set; }

        public EKayitlanmaTipi KayitlanmaTipi { get; set; }

        public int UyeID { get; set; }

        protected override void EMMap(IDOTableBuilder<Ogrenci> builder) {
            
            builder.MapsTo(x => { x.SchemaName("AK").TableName("Ogrenciler"); });
            builder.For(d => d.OgrenciNo).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(64);
            builder.For(d => d.KayitYili).IsTypeOf(EDataType.Int).IsRequired();
            builder.For(d => d.KayitlanmaTipi).IsTypeOf(EDataType.Enum).IsRequired();

            builder.ForeignKey(x => x.UyeID).References<Uye>(u => u.ID);
            builder.UniqueKey(x => x.UyeID);

        }

    }
}
