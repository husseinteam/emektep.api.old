﻿using DM.Domain.Objects;
using EM.Objects.DomainObjects.Core;

namespace EM.Objects.DomainObjects.Kurum {

    public class MYO : EMBase<MYO> {

        public string Adi { get; set; }

        public string Ilcesi { get; set; }

        public int UniversiteID { get; set; }

        protected override void EMMap(IDOTableBuilder<MYO> builder) {
            
            builder.MapsTo(x => { x.SchemaName("KR").TableName("MYOlar"); });
            builder.For(d => d.Adi).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(128);
            builder.For(d => d.Ilcesi).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(128);

            builder.ForeignKey(x => x.UniversiteID).References<Universite>(x => x.ID);

        }

    }
}
