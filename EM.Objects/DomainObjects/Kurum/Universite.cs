﻿using DM.Domain.Objects;
using EM.Objects.DomainObjects.Core;

namespace EM.Objects.DomainObjects.Kurum {

    public class Universite : EMBase<Universite> {

        public string Adi { get; set; }

        public string KurulusYeri { get; set; }

        protected override void EMMap(IDOTableBuilder<Universite> builder) {
            
            builder.MapsTo(x => { x.SchemaName("KR").TableName("Universiteler"); });
            builder.For(d => d.Adi).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(128);
            builder.For(d => d.KurulusYeri).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(128);

        }

    }
}
