﻿using DM.Domain.Data;
using EM.Entities.AggragateObjects.Tedrisat;
using EM.Objects.DomainObjects.Soru;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EM.Test.UnitTests {

    [TestClass]
    public class DatabaseTests {

        [TestMethod]
        public void BuildsDomain() {
            
            SBuildEngine.BuildDomain(typeof(Question).Assembly.GetName());

        }
        
        [TestMethod]
        public void SlugGeneratesRightType() {

            var type = SBuildEngine.BuildType(typeof(Question).Assembly.GetName(), "sorular");
            Assert.IsNotNull(type);

        }

    }

}
