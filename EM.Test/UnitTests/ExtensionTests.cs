﻿using System;
using System.Linq.Expressions;
using EM.Entities.DomainObjects.Mufredat;
using EM.Extension.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EM.Test.UnitTests {

    [TestClass]
    public class ExtensionTests {

        [TestMethod]
        public void ResultifyRunsOK() {


            var mcc = new MyClass() { i = 2 };
            Expression<Func<MyClass, bool>> expr = (mc) => mc.i == mcc.i;
            var operation = expr.Body as BinaryExpression;
            var result = operation.Right.Resultify();
            Assert.AreEqual(2, result);

        }

        [TestMethod]
        public void DescribeRunsOK() {

            var donem = EAkademikDonem.YazDonemi;
            Assert.AreEqual("yazogretimi", donem.Describe());

        }

        class MyClass {
            public int i { get; set; }
        }

    }

}
