﻿using System;
using DM.Domain.Data;
using DM.Domain.DML;
using DM.Domain.Objects;
using DM.Domain.Query;
using EM.Entities.DomainObjects.AkademikKadro;
using EM.Entities.DomainObjects.Uyelik;
using EM.Extension.Core;
using EM.Objects.DomainObjects.AkademikKadro;
using EM.Objects.DomainObjects.Kurum;
using EM.Objects.DomainObjects.Mufredat;
using EM.Objects.DomainObjects.Soru;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EM.Test.UnitTests {

    [TestClass]
    public class CRUDTests {

        [TestMethod]
        public void InsertChainRunsOK() {

            SBuildEngine.BuildDomain(typeof(Question).Assembly.GetName());

            CR<Uye>.CRUD(new Uye() { Contact = "lampiclobe@outlook.com", Password = "1q2w3e4r5t".GenerateMD5(), ActivationToken = 2.GenerateToken(), ActivationTokenExpiresOn = DateTime.Now.AddHours(1) }).Upsert(u => {
                Assert.IsNotNull(u);
                Assert.AreNotEqual(0, u.ID);
                CR<Universite>.CRUD(new Universite() { Adi = "Abant İzzet Baysal Üniversitesi", KurulusYeri = "Bolu" })
                    .Single(ibu => new MYO() { Adi = "Yeniçağa Yaşar Çelik Meslek Yüksekokulu", Ilcesi = "Yeniçağa", UniversiteID = ibu.Output.ID })
                    .Single(myo => new Program() { Adi = "Dış Ticaret", MYOID = myo.Output.ID })
                    .Single(prog => new Mufredat() { Adi = "2011 Müfredatı", YururlukYili = 2011, ProgramID = prog.Output.ID })
                    .Single(muf => new Donem() { DonemAdi = "I. Donem", MufredatID = muf.Output.ID })
                    .Sibling()
                        .Next(dnm => new Ders() { DersAdi = "Genel İşletme", DersKodu = "201223455", Kredi = 12, TeorikSaat = 12, UygulamaSaat = 2, DonemID = dnm.Output.ID })
                        .Next(dnm => new Ogrenci() {
                            OgrenciNo = 1.GenerateToken(),
                            KayitlanmaTipi = EKayitlanmaTipi.OSS,
                            KayitYili = 2015,
                            UyeID = u.ID
                        })
                        .Execute(cursor =>
                            CR<Exam>.CRUD(new Exam() {
                                Title = "Genel Test 1", ExamDate = DateTime.Now.AddMonths(1),
                                LectureID = cursor.FirstOccurrance<Ders>().ID,
                                StudentID = cursor.FirstOccurrance<Ogrenci>().ID
                            })
                            .Single(snv =>
                                new Question() {
                                    Title = "Nedir?",
                                    QuestionType = EQuestionType.Test, ExamID = snv.Output.ID
                                }
                            ).Sibling()
                                .Next(soru => new TestChoice() { Correct = false, Value = 0, Text = "Ne değildir ki", QuestionID = soru.Output.ID })
                                .Next(soru => new TestChoice() { Correct = false, Value = 1, Text = "Soru neydi", QuestionID = soru.Output.ID })
                                .Next(soru => new TestChoice() { Correct = true, Value = 2, Text = "O'dur", QuestionID = soru.Output.ID })
                                .Execute(result => {
                                    result.Parse<TestChoice>((tc) => Assert.IsNotNull(tc));
                                    result.Parse<TestChoice>((tc) => Assert.AreNotEqual(0, tc.Output.ID));
                                })
                        );
            }, (ex) => Assert.Fail(ex.Message));


        }

        [TestMethod]
        public void UpdateRunsOK() {

            Q<Uye>.SelectAllColumns().Where(u => u.TCKN == "25871077074").And(u => u.Password == "1q2w3e4r5t").ExecuteOne(cursor => {
                if (cursor.ResolvedEntity != null) {
                    var token = 1.GenerateToken();
                    var expireDate = DateTime.Now.AddMinutes(20);
                    cursor.Modify()
                        .Update(x => x.AccessTokenExpiresOn).Set(expireDate)
                        .Update(x => x.AccessToken).Set(token).PersistUpdate((rowc, entity) => {
                            Assert.IsNotNull(entity);
                            Assert.AreNotEqual("", entity.AccessToken);
                            Assert.AreEqual(token, entity.AccessToken);
                            Assert.AreEqual(expireDate, entity.AccessTokenExpiresOn);
                        });
                } else {
                    Assert.Fail("Uye not found");
                }
            });

        }
        [TestMethod]
        public void DeleteRunsOK() {

            Action<Exception> fallback = (exc) => Assert.Fail(exc.Message);
            Q<TestChoice>.SelectAllColumns().Where(x => x.ID == 1).ExecuteOne(xq => {
                xq.Delete(fallback);
            });
            Q<TestChoice>.SelectAllColumns().Where(x => x.ID == 2).ExecuteOne(xq => {
                xq.Delete(fallback);
            });
            Q<TestChoice>.SelectAllColumns().Where(x => x.ID == 3).ExecuteOne(xq => {
                xq.Delete(fallback);
            });
            Q<Question>.SelectAllColumns().Where(x => x.ID == 1).ExecuteOne(xq => {
                xq.Delete(fallback);
            });
            Q<Exam>.SelectAllColumns().Where(x => x.ID == 1).ExecuteOne(xq => {
                xq.Delete(fallback);
            });
            Q<Ogrenci>.SelectAllColumns().Where(x => x.ID == 1).ExecuteOne(xq => {
                xq.Delete(fallback);
            });
            Q<Ders>.SelectAllColumns().Where(x => x.ID == 1).ExecuteOne(xq => {
                xq.Delete(fallback);
            });
            Q<Donem>.SelectAllColumns().Where(x => x.ID == 1).ExecuteOne(xq => {
                xq.Delete(fallback);
            });
            Q<Mufredat>.SelectAllColumns().Where(x => x.ID == 1).ExecuteOne(xq => {
                xq.Delete(fallback);
            });
            Q<Program>.SelectAllColumns().Where(x => x.ID == 1).ExecuteOne(xq => {
                xq.Delete(fallback);
            });
            Q<MYO>.SelectAllColumns().Where(x => x.ID == 1).ExecuteOne(xq => {
                xq.Delete(fallback);
            });
            Q<Universite>.SelectAllColumns().Where(x => x.ID == 1).ExecuteOne(xq => {
                xq.Delete(fallback);
            });

        }

    }

}
