﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using DM.Domain.Data;
using DM.Domain.DML;
using DM.Domain.Objects;
using EM.Entities.DomainObjects.AkademikKadro;
using EM.Entities.DomainObjects.Karsilama;
using EM.Entities.DomainObjects.Mufredat;
using EM.Entities.DomainObjects.Uyelik;
using EM.Extension.Core;
using EM.Objects.DomainObjects.AkademikKadro;
using EM.Objects.DomainObjects.Kurum;
using EM.Objects.DomainObjects.Mufredat;
using EM.Objects.DomainObjects.Soru;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EM.Test.UnitTests {

    [TestClass]
    public class SeedTests {

        [TestMethod]
        public void SeedsDatabase() {

            SBuildEngine.BuildDomain(typeof(Question).Assembly.GetName());

            IChild<MYO> yycmyo = null;
            Fakulte tipFakultesi = null;
            var ibu = CR<Universite>.CRUD(new Universite() { Adi = "Abant İzzet Baysal Üniversitesi", KurulusYeri = "Bolu" })
                .Sibling()
                    .Next(ibueq => new Fakulte() { Adi = "Tıp Fakültesi", UniversiteID = ibueq.Output.ID })
                    .Next(ibueq => new MYO() { Adi = "Yeniçağa Yaşar Çelik Meslek Yüksekokulu", Ilcesi = "Yeniçağa", UniversiteID = ibueq.Output.ID })
                    .Next(ibueq => new MYO() { Adi = "Mudurnu Süreyya Astarcı M.Y.O.", Ilcesi = "Mudurnu", UniversiteID = ibueq.Output.ID })
                    .Execute(results => {
                        yycmyo = new Child<MYO>(results.FirstOccurrance<MYO>());
                        tipFakultesi = results.FirstOccurrance<Fakulte>();
                    });

            SeedProgram(yycmyo);
            SeedDersler(yycmyo);
            SeedAkademikTakvim(ibu);
            SeedAkademikTakvimTip(ibu, tipFakultesi);
            SeedDuyurular();

        }
        
        private static void SeedExamOnCursor(int lectureID, int studentID) {
            var exam = CR<Exam>.CRUD(new Exam() {
                Title = 3.SpeakSentence(),
                ExamDate = DateTime.Now.AddMonths(1),
                LectureID = lectureID,
                StudentID = studentID
            });

            for (int i = 0; i < 5; i++) {
                exam.Single(snv =>
                new Question() {
                    Title = 5.SpeakSentence(),
                    QuestionType = EQuestionType.Test,
                    ExamID = snv.Output.ID
                }).Sibling()
                    .Next(soru => new TestChoice() {
                        Correct = false,
                        Value = 0,
                        Text = 2.SpeakSentence(),
                        QuestionID = soru.Output.ID
                    })
                    .Next(soru => new TestChoice() {
                        Correct = false,
                        Value = 0,
                        Text = 2.SpeakSentence(),
                        QuestionID = soru.Output.ID
                    })
                    .Next(soru => new TestChoice() {
                        Correct = false,
                        Value = 0,
                        Text = 2.SpeakSentence(),
                        QuestionID = soru.Output.ID
                    })
                    .Next(soru => new TestChoice() {
                        Correct = false,
                        Value = 0,
                        Text = 2.SpeakSentence(),
                        QuestionID = soru.Output.ID
                    })
                    .Execute(result => {
                        result.Parse<TestChoice>((tc) => Assert.IsNotNull(tc));
                        result.Parse<TestChoice>((tc) => Assert.AreNotEqual(0, tc.Output.ID));
                    });
            }
            
        }

        private static void SeedDersler(IChild<MYO> yycmyo) {
            List<IExecutedResultList> dersCursors = new List<IExecutedResultList>();
            yycmyo
                .Single(myor => new Program() {
                    Adi = "Dış Ticaret",
                    MYOID = myor.Output.ID
                })
                .Single(myo => new Uye() {
                    Contact = "lampiclobe@outlook.com".Scatter(5.RandomDigits()),
                    Password = "1q2w3e4r5t".GenerateMD5(),
                    FullName = "Özgür Sönmez",
                    TCKN = "25871077074"
                }).Single((uy) => new OgretimUyesi() {
                    Unvani = "Dr.",
                    UyeID = uy.Output.ID
                })
                .ExecuteMany(loginsMember => {
                    OleDbSeed("ders-listesi.xlsx", "rptDersListesi", (command) => {
                        using (var reader = command.ExecuteReader()) {
                            for (int i = 0; i < 4; i++) {
                                reader.Read();
                            }
                            while (reader.Read()) {
                                yycmyo
                                    .Single(myo => new Uye() {
                                        Contact = "lampiclobe-{0}@outlook.com".Scatter(5.RandomDigits()),
                                        Password = "1q2w3e4r5t".GenerateMD5(),
                                        FullName = reader.GetCellAt<string>(2),
                                        TCKN = 11.RandomDigits()
                                    })
                                    .Single((uy) => new OgretimUyesi() {
                                        Unvani = reader.GetCellAt<string>(2).Split(' ')
                                            .Reverse().Skip(2).First(),
                                        UyeID = uy.Output.ID
                                    })
                                    .ExecuteMany(cursor => {
                                        var prog = loginsMember.FirstOccurrance<Program>();
                                        var ogrtu = loginsMember.FirstOccurrance<OgretimUyesi>();
                                        CR<DersYuku>.CRUD(new DersYuku() {
                                            Baslik = "Ders Yükü",
                                            OgretimUyesiID = ogrtu.ID,
                                            ProgramID = prog.ID
                                        }).Upsert(dy => { Assert.IsNotNull(dy); Assert.AreNotEqual(0, dy.ID); });
                                    });
                                yycmyo.Single(prog => new Mufredat() { Adi = "2011 Müfredatı", YururlukYili = 2011, ProgramID = prog.Output.ID })
                                .Single(muf => new Donem() { DonemAdi = "I. Donem", MufredatID = muf.Output.ID })
                                .Single(dnm => new Ders() {
                                    DersAdi = reader.GetCellAt<string>(1),
                                    DersKodu = reader.GetCellAt<string>(0),
                                    TeorikSaat = reader.GetCellAt<int>(3),
                                    UygulamaSaat = reader.GetCellAt<int>(4),
                                    Kredi = reader.GetCellAt<int>(5),
                                    DonemID = dnm.Output.ID
                                })
                                .ExecuteMany(cursor => {
                                    dersCursors.Add(cursor);
                                    Assert.IsNotNull(cursor);
                                });
                            }
                        }
                    });
                    OleDbSeed("ogrenci-listesi.xlsx", "rptSinifListesi", (command) => {
                        using (var reader = command.ExecuteReader()) {
                            for (int i = 0; i < 14; i++) {
                                reader.Read();
                            }
                            while (reader.Read()) {
                                yycmyo
                                    .Single((prg) => new Uye() {
                                        Contact = "lampiclobe-{0}@outlook.com".Scatter(5.RandomDigits()),
                                        Password = "1q2w3e4r5t".GenerateMD5(),
                                        FullName = reader.GetCellAt<string>(4) + " " +
                                            reader.GetCellAt<string>(7),
                                        TCKN = 11.RandomDigits()
                                    }).ExecuteMany(cursor => {
                                        var prog = loginsMember.FirstOccurrance<Program>();
                                        var ogrtu = loginsMember.FirstOccurrance<OgretimUyesi>();
                                        var uye = cursor.FirstOccurrance<Uye>();
                                        CR<DersYuku>.CRUD(new DersYuku() { Baslik = "Ders-Yuku-{0}".Scatter(1.RandomDigits()), OgretimUyesiID = ogrtu.ID, ProgramID = prog.ID })
                                        .Single(dy => new Ogrenci() {
                                            OgrenciNo = reader.GetCellAt<string>(2),
                                            KayitlanmaTipi = EKayitlanmaTipi.OSS,
                                            KayitYili = 2015,
                                            UyeID = uye.ID
                                        })
                                        .ExecuteMany(cur => {
                                            Assert.IsNotNull(cur);
                                            var dyuku = cur.FirstOccurrance<DersYuku>();
                                            var ogr = cur.FirstOccurrance<Ogrenci>();
                                            SeedExamOnCursor(
                                                dersCursors.First().FirstOccurrance<Ders>().ID, 
                                                cur.FirstOccurrance<Ogrenci>().ID);
                                            CR<DersProgrami>.CRUD(new DersProgrami() { Baslik = "Program-1", DersYukuID = dyuku.ID, OgrenciID = ogr.ID })
                                            .ExecuteMany(cu => { Assert.IsNotNull(cu); });
                                        });

                                    });
                            }
                        }
                    });

                });
        }

        private static void SeedProgram(IChild<MYO> yycmyo) {

            using (var con = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=""{0}"";Extended Properties = 'text;HDR=Yes;FMT=Delimited(,)'; "
            .Scatter(Environment.CurrentDirectory + @"\Assets"))) {
                using (var cmd = new OleDbCommand("SELECT * FROM [program-listesi.csv]", con)) {
                    cmd.Connection.Open();
                    using (OleDbDataReader reader = cmd.ExecuteReader()) {
                        while (reader.Read()) {
                            yycmyo.Single(myo => new Program() {
                                Adi = reader.GetCellAt<string>(0),
                                MYOID = myo.Output.ID
                            });
                        }
                    }

                }
            }

        }

        private static void SeedAkademikTakvim(Universite universite) {

            new[] { EAkademikDonem.BirinciYariyil, EAkademikDonem.IkinciYariyil, EAkademikDonem.YazDonemi }.Each(donem => {
                OleDbSeed("academic-calendar.xlsx", donem.Describe(), (command) => {
                    using (OleDbDataReader reader = command.ExecuteReader()) {
                        reader.Read();
                        while (reader.Read()) {
                            CR<AkademikTakvim>.CRUD(new AkademikTakvim() {
                                Baslangic = reader.GetCellAt<DateTime>(0),
                                Bitis = reader.GetCellAt<DateTime>(1),
                                Aciklama = reader.GetCellAt<string>(2),
                                AkademikDonem = donem,
                                FakulteID = null
                            }).Upsert(at => { Assert.IsNotNull(at); Assert.AreNotEqual(0, at.ID); }, exc => Assert.Fail(exc.Message));
                        }
                    }
                });
            });

        }

        private static void SeedAkademikTakvimTip(Universite universite, Fakulte tip) {
            new[] { EAkademikDonem.Donem1, EAkademikDonem.Donem2, EAkademikDonem.Donem3, EAkademikDonem.Donem4, EAkademikDonem.Donem5, EAkademikDonem.Donem6 }
                .Each(donem => {
                    OleDbSeed("academic-calendar-medicine.xlsx", donem.Describe(), (command) => {
                        using (OleDbDataReader reader = command.ExecuteReader()) {
                            reader.Read();
                            while (reader.Read()) {
                                CR<AkademikTakvim>.CRUD(new AkademikTakvim() {
                                    Baslangic = reader.GetCellAt<DateTime>(0),
                                    Bitis = reader.GetCellAt<DateTime>(1),
                                    Aciklama = reader.GetCellAt<string>(2),
                                    AkademikDonem = donem,
                                    FakulteID = tip.ID
                                }).Upsert(at => { Assert.IsNotNull(at); Assert.AreNotEqual(0, at.ID); }, exc => Assert.Fail(exc.Message));
                            }
                        }
                    });
                });

        }

        private static void SeedDuyurular() {

            OleDbSeed("duyuru-listesi.xlsx", "Sayfa1", (command) => {
                using (OleDbDataReader reader = command.ExecuteReader()) {
                    reader.Read();
                    reader.Meanwhile(() => reader.Read(), (rd, i) => {
                        new[] { EBulletinType.Activity, EBulletinType.Announcement, EBulletinType.News }.Each(tip => {
                            CR<Bulletin>.CRUD(new Bulletin() {
                                Baslik = rd.GetCellAt<string>(0),
                                YayimTarihi = rd.GetCellAt<DateTime>(1),
                                OnYazi = rd.GetCellAt<string>(2),
                                Etiketler = rd.GetCellAt<string>(3),
                                BaslikResmi = @"http://app.lampiclobe.com/Public/Landing/Bulletins/duyuru-{0}.jpg".Scatter(i),
                                Tip = tip
                            }).Upsert(at => { Assert.IsNotNull(at); Assert.AreNotEqual(0, at.ID); }, exc => Assert.Fail(exc.Message));
                        });
                    });
                }
            });

        }

        private static void OleDbSeed(string fileName, string sheetName, Action<OleDbCommand> commander) {
            using (var connection = new OleDbConnection(
                @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=""Excel 12.0 Xml; HDR = YES"";".Scatter(
                    Environment.CurrentDirectory + @"\Assets\{0}".Scatter(fileName)))) {
                using (var command = new OleDbCommand("Select * FROM [{0}$]".Scatter(sheetName), connection)) {
                    command.Connection.Open();
                    commander(command);
                    command.Connection.Close();
                }

            }
        }

    }
}
