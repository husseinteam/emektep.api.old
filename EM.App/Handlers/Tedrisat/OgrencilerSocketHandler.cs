﻿using DM.Domain.Aggregate;
using DM.Domain.Query;
using EM.App.Handlers.Base;
using EM.Entities.AggragateObjects.Tedrisat;
using EM.Entities.DomainObjects.Uyelik;
using EM.Entities.VectorObjects;
using EM.Extension.Core;

namespace EM.App.Handlers.Tedrisat {

    class OgrencilerSocketHandler : BaseWebSocketHandler {

        private readonly int _DersID;

        public OgrencilerSocketHandler(int dersID) {

            this._DersID = dersID;
            Authorized += OgrenciListesiSocketHandler_Authorized;

        }

        private void OgrenciListesiSocketHandler_Authorized(IExecutedQuery<Uye> cursor) {

            var uye = cursor.ResolvedEntity;
            AG<OgrenciListesiAG>.Views().SetDistinctColumn(x => x.DersID).SelectAllColumns().Where(ou => ou.OgretimUyesiUyeID == uye.ID && ou.DersID == this._DersID).ExecuteList(c => {
                this.Send(new MessageVector() {
                    done = true,
                    data = c
                }.ToJson());
            }, () => {
                this.Close();
            });

        }
    }

}