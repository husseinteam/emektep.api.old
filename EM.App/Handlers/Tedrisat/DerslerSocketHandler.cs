﻿using DM.Domain.Aggregate;
using DM.Domain.Query;
using EM.App.Handlers.Base;
using EM.App.Messengers.Membership;
using EM.Entities.AggragateObjects.Tedrisat;
using EM.Entities.DomainObjects.Uyelik;
using EM.Entities.VectorObjects;
using EM.Extension.Core;

namespace EM.App.Handlers.Tedrisat {

    class DerslerSocketHandler : BaseWebSocketHandler {

        public DerslerSocketHandler() {

            Authorized += DersListesiSocketHandler_Authorized;

        }

        private void DersListesiSocketHandler_Authorized(IExecutedQuery<Uye> cursor) {

            DerslerMessenger.Generate(cursor,
                mvect => this.Send(mvect.ToJson()),
                () => {
                    this.Close();
                },
                () => {
                    this.Close();
                });

        }
    }

}