﻿using DM.Domain.Aggregate;
using DM.Domain.Query;
using EM.App.Handlers.Base;
using EM.App.Messengers.Tedrisat;
using EM.App.Messengers.Membership;
using EM.Entities.AggragateObjects.Tedrisat;
using EM.Entities.DomainObjects.Uyelik;
using EM.Entities.VectorObjects;
using EM.Extension.Core;

namespace EM.App.Handlers.Tedrisat {

    class AkademikTakvimSocketHandler : BaseWebSocketHandler {

        public AkademikTakvimSocketHandler() {

            Authorized += AkademikTakvimSocketHandler_Authorized; ;

        }

        private void AkademikTakvimSocketHandler_Authorized(IExecutedQuery<Uye> cursor) {

            AkademikTakvimMessenger.Generate(cursor,
                mvect => this.Send(mvect.ToJson()),
                () => {
                    this.Close();
                },
                () => {
                    this.Close();
                });

        }

    }

}