﻿using System;
using System.Collections.Generic;
using DM.Domain.Query;
using EM.Entities.DomainObjects.Uyelik;
using EM.Entities.VectorObjects;
using EM.Extension.Core;
using Microsoft.Web.WebSockets;

namespace EM.App.Handlers.Base {

    class BaseMembershipSocketHandler : WebSocketHandler {

        public void SendAndClose(string message) {

            this.Send(message);
            this.Close();

        }

    }

}