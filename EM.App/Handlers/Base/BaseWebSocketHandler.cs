﻿using System;
using System.Collections.Generic;
using DM.Domain.Query;
using EM.Entities.DomainObjects.Uyelik;
using EM.Entities.VectorObjects;
using EM.Extension.Core;
using Microsoft.Web.WebSockets;

namespace EM.App.Handlers.Base {

    class BaseWebSocketHandler : BaseMembershipSocketHandler {

        protected static List<WebSocketHandler> _Clients = new List<WebSocketHandler>();

        public BaseWebSocketHandler() {
            Unauthorized += (message) => {
                this.Send(new MessageVector() {
                    done = false,
                    messages = new[] { message }
                }.ToJson());
            };
        }

        public override void OnOpen() {
            _Clients.Add(this);
        }

        public sealed override void OnMessage(string message) {

            var mvect = message.FromJson<TokenVector>();
            if (mvect == null || mvect.token == null) {
                OnUnauthorized("Oturum Açmış Olmanız Gerekiyor.");
                return;
            }
            Q<Uye>.SelectAllColumns().Where(u => u.AccessToken == mvect.token)
                .ExecuteOne(cursor => {
                    if (DateTime.Now < cursor.ResolvedEntity.AccessTokenExpiresOn) {
                        Authorized(cursor);
                    } else {
                        cursor.Modify()
                           .Update(x => x.AccessTokenExpiresOn).Set(DateTime.MinValue)
                           .Update(x => x.AccessToken).Set("").PersistUpdate((rowc, entity) => {
                               this.SendAndClose(new MessageVector() {
                                   done = false,
                                   token = "",
                                   messages = new[] { "Yeniden Giriş Yapmalısınız." }
                               }.ToJson());
                           });
                    }
                }, () => OnUnauthorized("Oturum Anahtarı Geçersiz."));
        }

        public override void OnClose() {
            _Clients.Remove(this);
        }

        protected delegate void AuthorizedCursorHandle(IExecutedQuery<Uye> cursor);
        protected delegate void UnauthorizedCursorHandle(string message);
        protected event AuthorizedCursorHandle Authorized;
        protected event UnauthorizedCursorHandle Unauthorized;
        protected void OnAuthorized(IExecutedQuery<Uye> cursor) {
            if (Authorized != null) {
                Authorized(cursor);
            }
        }
        protected void OnUnauthorized(string message) {
            if (Unauthorized != null) {
                Unauthorized(message);
            }
        }

    }

}