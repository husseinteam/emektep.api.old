﻿using System;
using System.Configuration;
using System.Net.Mail;
using System.Web;
using DM.Domain.Query;
using EM.App.Handlers.Base;
using EM.App.HttpTools;
using EM.App.Messengers.Membership;
using EM.Entities.DomainObjects.Uyelik;
using EM.Entities.VectorObjects;
using EM.Extension.Core;
using EM.Service.Container;
using EM.Service.Contract;
using EM.Service.Enum;
using Microsoft.Web.WebSockets;
using Newtonsoft.Json;

namespace EM.App.Handlers.Membership {

    class RecoverSocketHandler : BaseMembershipSocketHandler {

        public override void OnMessage(string message) {

            RecoverMessenger.Generate(message, (msg) => this.SendAndClose(msg.ToJson()));

        }

    }

}