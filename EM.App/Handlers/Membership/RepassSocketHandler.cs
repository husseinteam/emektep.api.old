﻿using System;
using DM.Domain.Query;
using EM.Entities.DomainObjects.Uyelik;
using EM.Entities.VectorObjects;
using EM.Extension.Core;
using Microsoft.Web.WebSockets;
using Newtonsoft.Json;
using EM.App.HttpTools;
using EM.App.Handlers.Base;
using EM.App.Messengers.Membership;

namespace EM.App.Handlers.Membership {

    class RepassSocketHandler : BaseMembershipSocketHandler {

        public override void OnMessage(string message) {

            RepassMessenger.Generate(message, (msg) => this.SendAndClose(msg.ToJson()));

        }

    }

}