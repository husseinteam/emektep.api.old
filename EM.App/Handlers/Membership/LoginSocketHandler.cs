﻿using System;
using DM.Domain.Query;
using EM.App.Handlers.Base;
using EM.App.Messengers.Membership;
using EM.Entities.DomainObjects.Uyelik;
using EM.Entities.VectorObjects;
using EM.Extension.Core;
using Microsoft.Web.WebSockets;
using Newtonsoft.Json;

namespace EM.App.Handlers.Membership {

    class LoginSocketHandler : BaseMembershipSocketHandler {

        public override void OnMessage(string message) {

            LoginMessenger.Generate(message, (msg) => this.SendAndClose(msg.ToJson()));

        }

    }

}