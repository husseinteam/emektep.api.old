﻿using DM.Domain.Aggregate;
using DM.Domain.Query;
using EM.App.Handlers.Base;
using EM.App.Messengers.Landing;
using EM.App.Messengers.Membership;
using EM.Entities.AggragateObjects.Tedrisat;
using EM.Entities.DomainObjects.Karsilama;
using EM.Entities.DomainObjects.Uyelik;
using EM.Entities.VectorObjects;
using EM.Extension.Core;

namespace EM.App.Handlers.Landing {

    class EtkinlikSocketHandler : BaseWebSocketHandler {

        public EtkinlikSocketHandler() {

            Authorized += EtkinlikSocketHandler_Authorized; ;

        }

        private void EtkinlikSocketHandler_Authorized(IExecutedQuery<Uye> cursor) {

            BulletinMessenger.Generate(EBulletinType.Activity, cursor,
                mvect => this.Send(mvect.ToJson()),
                () => {
                    this.Close();
                }, () => {
                    this.Close();
                });

        }

    }

}