﻿using DM.Domain.Objects;
using DM.Domain.Query;
using EM.App.Handlers.Base;
using EM.Entities.VectorObjects;
using EM.Extension.Core;

namespace EM.App.Handlers.Generic {

    class QSocketHandler<TEntity> : BaseWebSocketHandler
        where TEntity : DOBase<TEntity>, new() {

        public QSocketHandler() {

            Authorized += (uye) => {
                Q<TEntity>.SelectAllColumns().ExecuteMany(exq => {
                    this.Send(new MessageVector() {
                        done = true,
                        data = exq.ResolvedEntity,
                        messages = new string[] { "listed" }
                    }.ToJson());
                });
            };

        }


        public QSocketHandler(int id) {

            Authorized += (uye) => {
                Q<TEntity>.SelectAllColumns().Where(e => e.ID == id).ExecuteOne(exq => {
                    this.Send(new MessageVector() {
                        done = true,
                        data = exq.ResolvedEntity,
                        messages = new string[] { "listed" }
                    }.ToJson());
                });
            };
            
        }

    }

}