﻿using System;
using System.Collections.Generic;
using DM.Domain.Objects;
using DM.Domain.Query;
using EM.App.Handlers.Base;
using EM.Entities.VectorObjects;
using EM.Extension.Core;

namespace EM.App.Handlers.Generic {

    class QApiHandler<TEntity> : IQSelector
        where TEntity : DOBase<TEntity>, new() {

        public List<MessageVector> SelectAll() {
            var messages = new List<MessageVector>();
            Q<TEntity>.SelectAllColumns().ExecuteMany(exq => {
                messages.Add(new MessageVector() {
                    done = true,
                    data = exq.ResolvedEntity,
                    messages = new string[] { "listed" }
                });
            });
            return messages;
        }

        public List<MessageVector> SelectThe(int id) {
            var messages = new List<MessageVector>();
            Q<TEntity>.SelectAllColumns().Where(e => e.ID == id).ExecuteOne(exq => {
                messages.Add(new MessageVector() {
                    done = true,
                    data = exq.ResolvedEntity,
                    messages = new string[] { "listed" }
                });
            });
            return messages;
        }

    }

    interface IQSelector {
        List<MessageVector> SelectAll();
        List<MessageVector> SelectThe(int id);
    }

}