﻿using DM.Domain.Aggregate;
using EM.App.Handlers.Base;
using EM.Entities.VectorObjects;
using EM.Extension.Core;

namespace EM.App.Handlers.Generic {

    class AGSocketHandler<T> : BaseWebSocketHandler
        where T : AGBase<T>, new() {

        public AGSocketHandler() {

            Authorized += (uye) => {
                AG<T>.Views().SelectAllColumns().ExecuteList(cursor => {
                    this.Send(new MessageVector() {
                        done = true,
                        data = cursor,
                        messages = new string[] { "listed" }
                    }.ToJson());
                }, ()=> {
                    this.Close();
                });
            };

        }

    }

}