﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using EM.Extension.Core;

namespace EM.App.HttpTools {

    internal class FileResult : IHttpActionResult {

        private readonly FileInfo _filePath;
        private readonly string _contentType;

        internal string FileName {
            get {
                return _filePath.ToString()
                    .Replace(HttpContext.Current.Server.MapPath("~"), "")
                    .Replace(Path.DirectorySeparatorChar, '/');
            }

        }

        internal string FileUrl {
            get {
                return HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + "/" + FileName;
            }
        }

        internal byte[] FileBytes {
            get {
                return File.OpenRead(_filePath.ToString()).ReadAllBytes();
            }
        }

        internal string FilePath { get { return _filePath.ToString(); } }

        internal void CloseUnderlyingFile(string handlePath) {
            _filePath.CloseFile(handlePath);
        }

        internal string ContentType { get { return _contentType ?? MimeMapping.GetMimeMapping(Path.GetExtension(_filePath.ToString())); } }

        internal FileResult(FileInfo file, string contentType = null) {
            _filePath = file;
            _contentType = contentType;
        }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken) {

            var bytes = FileBytes;
            var response = new HttpResponseMessage(HttpStatusCode.OK) {
                Content = new ByteArrayContent(bytes)
            };
            
            response.Content.Headers.ContentType = new MediaTypeHeaderValue(ContentType);
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            response.Content.Headers.ContentDisposition.FileName = FileName;
            response.Content.Headers.ContentLength = bytes.Length;
            return Task.FromResult(response);

        }
        
    }

}