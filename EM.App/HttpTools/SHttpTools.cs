﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using EM.Extension.Core;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Novacode;

namespace EM.App.HttpTools {

    internal static class SHttpTools {

        internal static string AddHttpPrefix(this HttpRequest request, string url) {

            return "{0}://{1}/{2}".Scatter(request.Url.Scheme,
                ConfigurationManager.AppSettings["WebAppAuthority"], url);

        }

        internal static IHttpActionResult GeneratePdfDocument(JToken dict,
            Func<FileResult, IHttpActionResult> doneback,
            Func<Exception, IHttpActionResult> fallback = null) {

            FileInfo pdffi = null;
            try {
                pdffi = new FileInfo(Path.Combine(
                    HttpContext.Current.Server.MapPath("~/Public/Exports"),
                    "{1}-{0}.{2}".Scatter(Guid.NewGuid().ToString("N"),
                        dict.FromJToken<string>("ExportTitle"), "pdf")));
                foreach (var pdf in Directory.EnumerateFiles(HttpContext.Current.Server.MapPath("~/Public/Exports"), "*.{0}".Scatter("pdf"))) {
                    new FileInfo(pdf).Delete();
                }
                Document document = new Document();
                PdfWriter.GetInstance(document, new FileStream(pdffi.ToString(), FileMode.OpenOrCreate, FileAccess.ReadWrite));
                document.Open();

                var columns = dict.FromJToken<JArray>("TableColumns");
                var rows = dict.FromJToken<JArray>("TableRows");

                PdfPTable table = new PdfPTable(columns.Count);
                for (var i = 1; i <= columns.Count(); i++) {
                    table.AddCell(columns.ElementAt(i - 1)
                        .FromJToken<string>("headerText"));
                }
                for (var j = 1; j <= rows.Count(); j++) {
                    for (var i = 1; i <= columns.Count(); i++) {
                        var colName = columns.ElementAt(i - 1).FromJToken<string>("rowText");
                        table.AddCell(rows.ElementAt(j - 1)[colName].ToString());
                    }
                }

                document.Add(table);
                document.Close();
                return doneback(new FileResult(pdffi));
            } catch (Exception ex) {
                return fallback(ex);
            }

        }

        internal static IHttpActionResult GenerateExcelDocument(JToken dict,
            Func<FileResult, IHttpActionResult> doneback,
            Func<Exception, IHttpActionResult> fallback) {
            var engine = new ExcelPackage();
            FileInfo excelfi = null;
            try {
                var workSheet = engine.Workbook.Worksheets
                    .Add(dict.FromJToken<string>("WorksheetName"));
                var columns = dict.FromJToken<JArray>("TableColumns");
                var rows = dict.FromJToken<JArray>("TableRows");

                for (var i = 1; i <= columns.Count(); i++) {
                    workSheet.Cells[1, i].Value = columns.ElementAt(i - 1)
                        .FromJToken<string>("headerText");
                }
                for (var j = 1; j <= rows.Count(); j++) {
                    for (var i = 1; i <= columns.Count(); i++) {
                        var colName = columns.ElementAt(i - 1).FromJToken<string>("rowText");
                        workSheet.Cells[j + 1, i].Value = rows.ElementAt(j - 1)[colName].ToString();
                    }
                }
                excelfi = new FileInfo(Path.Combine(
                    HttpContext.Current.Server.MapPath("~/Public/Exports"),
                    "{1}-{0}.{2}".Scatter(Guid.NewGuid().ToString("N"),
                        dict.FromJToken<string>("ExportTitle"), "xlsx")));
                foreach (var xlsx in Directory.EnumerateFiles(HttpContext.Current.Server.MapPath("~/Public/Exports"), "*.{0}".Scatter("xlsx"))) {
                    new FileInfo(xlsx).Delete();
                }
                engine.SaveAs(excelfi);
            } catch (Exception ex) {
                return fallback(ex);
            }

            return doneback(new FileResult(excelfi));

        }

        internal static IHttpActionResult GenerateWordDocument(JToken dict,
            Func<FileResult, IHttpActionResult> doneback,
            Func<Exception, IHttpActionResult> fallback = null) {

            FileInfo docxfi = null;
            try {
                docxfi = new FileInfo(Path.Combine(
                    HttpContext.Current.Server.MapPath("~/Public/Exports"),
                    "{1}-{0}.{2}".Scatter(Guid.NewGuid().ToString("N"),
                        dict.FromJToken<string>("ExportTitle"), "docx")));
                foreach (var pdf in Directory.EnumerateFiles(HttpContext.Current.Server.MapPath("~/Public/Exports"), "*.{0}".Scatter("docx"))) {
                    new FileInfo(pdf).Delete();
                }
                var doc = DocX.Create(docxfi.ToString());

                var rows = dict.FromJToken<JArray>("TableRows");
                var columns = dict.FromJToken<JArray>("TableColumns");

                var table = doc.AddTable(1, columns.Count);
                var headerRow = table.Rows.First();
                for (var i = 1; i <= columns.Count(); i++) {
                    headerRow.Cells[i - 1].InsertParagraph(columns.ElementAt(i - 1)
                        .FromJToken<string>("headerText"));
                }
                for (var j = 1; j <= rows.Count(); j++) {
                    var row = table.InsertRow();
                    for (var i = 1; i <= columns.Count(); i++) {
                        var colName = columns.ElementAt(i - 1).FromJToken<string>("rowText");
                        row.Cells[i - 1].InsertParagraph(rows.ElementAt(j - 1)[colName].ToString());
                    }
                }
                doc.InsertTable(table);
                doc.Save();
                return doneback(new FileResult(docxfi));
            } catch (Exception ex) {
                return fallback(ex);
            }

        }

    }

}