﻿using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EM.App.Controllers {

    /// <summary>
    /// Error Controller
    /// </summary>
    public class ErrorController : ApiController {

        /// <summary>
        /// Get
        /// </summary>
        /// <returns>HttpResponseMessage</returns>
        [AllowAnonymous]
        [ActionName("Get")]
        public HttpResponseMessage Get() {
            return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, 
                message: "Program'da Beklenmedik Hata");
        }

        /// <summary>
        /// NotFound
        /// </summary>
        /// <returns>HttpResponseMessage</returns>
        [AllowAnonymous]
        [ActionName("404")]
        [HttpGet]
        public HttpResponseMessage Status404() {
            return Request.CreateErrorResponse(HttpStatusCode.NotFound, 
                message: "Mevcut Olmayan Sayfa.");
        }

        /// <summary>
        /// BadRequest
        /// </summary>
        /// <returns>HttpResponseMessage</returns>
        [AllowAnonymous]
        [ActionName("400")]
        [HttpGet]
        public HttpResponseMessage Status400() {
            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, 
                message: "İşlenemeyen İstek");
        }

        /// <summary>
        /// InternalServerError
        /// </summary>
        /// <returns>HttpResponseMessage</returns>
        [AllowAnonymous]
        [ActionName("500")]
        [HttpGet]
        public HttpResponseMessage Status500() {
            return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, 
                message: "Program'da Beklenmedik Hata");
        }

    }

}
