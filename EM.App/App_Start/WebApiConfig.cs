﻿using System;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Mvc;
using EM.Extension.Core;

namespace EM.App {
    public static class WebApiConfig {
        public static void Register(HttpConfiguration config) {

            // Web API configuration and services
            var cors = new EnableCorsAttribute("*", "*", "*");
            cors.PreflightMaxAge = 600;
            config.EnableCors(cors);

            // Web API routes
            config.MapHttpAttributeRoutes();

            // ASP.NET MVC Areas
            AreaRegistration.RegisterAllAreas();

            //Static Files
            Action<string, string, string> staticifier = new Action<string, string, string>((name, subdir, pattern) => {
                //config.Routes.IgnoreRoute(name, "{{0}}".Scatter(pattern));
                config.Routes.MapHttpRoute(
                    name: "Static{0}Route".Scatter(name),
                    routeTemplate: "Public/{0}/{{1}}".Scatter(name, pattern)
                );
            });
            staticifier("Excel", "Exports", "*.xlsx");
            staticifier("Pdf", "Exports", "*.pdf");
            staticifier("Word", "Exports", "*.docx");
            staticifier("Jpeg", "Landing/Bulletins", "*.jpg");

            config.Routes.MapHttpRoute(
                name: "DefaultCatchall",
                routeTemplate: "{*url}",
                defaults: new {
                    controller = "Error",
                    action = "404"
                }
            );
        }
    }
}
