﻿using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.WebSockets;
using EM.App.Handlers.Landing;
using EM.App.Handlers.Tedrisat;

namespace EM.App.Areas.ws.Controllers {

    /// <summary>
    /// Karşılama Sayfası Socket Fonksiyonlarını İhtiva Eder
    /// </summary>
    [RoutePrefix("ws/karsilama")]
    public class LandingSocketController : BaseWebSocketController {

        /// <summary>
        /// AkademikTakvim i Ihtiva Eder
        /// </summary>
        /// <returns>Web Socket Cevabı içinde Akademik Takvimi Döndürür</returns>
        [Route("akademik-takvim")]
        [HttpGet]
        public HttpResponseMessage AkademikTakvim() {

            return ConstructWebSocketResponse(AkademikTakvimSocket);

        }

        private Task AkademikTakvimSocket(AspNetWebSocketContext context) {

            return new AkademikTakvimSocketHandler().ProcessWebSocketRequestAsync(context);

        }

        /// <summary>
        /// Duyurları İşler
        /// </summary>
        /// <returns>Web Socket Cevabı içinde Duyuruları Döndürür</returns>
        [Route("duyurular")]
        [HttpGet]
        public HttpResponseMessage Duyuru() {

            return ConstructWebSocketResponse(DuyuruSocket);

        }

        private Task DuyuruSocket(AspNetWebSocketContext context) {

            return new DuyuruSocketHandler().ProcessWebSocketRequestAsync(context);

        }

        /// <summary>
        /// Haberleri İşler
        /// </summary>
        /// <returns>Web Socket Cevabı içinde Haberleri Döndürür</returns>
        [Route("haberler")]
        [HttpGet]
        public HttpResponseMessage Haber() {

            return ConstructWebSocketResponse(HaberSocket);

        }

        private Task HaberSocket(AspNetWebSocketContext context) {

            return new HaberSocketHandler().ProcessWebSocketRequestAsync(context);

        }

        /// <summary>
        /// Aktiviteleri İşler
        /// </summary>
        /// <returns>Web Socket Cevabı içinde Aktiviteleri Döndürür</returns>
        [Route("aktiviteler")]
        [HttpGet]
        public HttpResponseMessage Etkinlik() {

            return ConstructWebSocketResponse(EtkinlikSocket);

        }

        private Task EtkinlikSocket(AspNetWebSocketContext context) {

            return new EtkinlikSocketHandler().ProcessWebSocketRequestAsync(context);

        }

    }
}
