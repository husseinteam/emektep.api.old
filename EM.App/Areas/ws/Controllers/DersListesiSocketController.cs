﻿using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.WebSockets;

using EM.App.Handlers.Tedrisat;

namespace EM.App.Areas.ws.Controllers {

    /// <summary>
    /// DersListesi View Fonksiyonlarını İhtiva Eder
    /// </summary>
    [RoutePrefix("ws/derslistesi")]
    public class DersListesiSocketController : BaseWebSocketController {


        /// <summary>
        /// Ogretim Uyesine Ait Dersler
        /// </summary>
        /// <returns>WebSocket Cevabı Döndürür</returns>
        [Route("uye")]
        [HttpGet]
        public HttpResponseMessage OgretimUyesineAitDersler() {

            return ConstructWebSocketResponse(AGSocket);

        }

        private Task AGSocket(AspNetWebSocketContext context) {

            return new DerslerSocketHandler().ProcessWebSocketRequestAsync(context);

        }

    }
}
