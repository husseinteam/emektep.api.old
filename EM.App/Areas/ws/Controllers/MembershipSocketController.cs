﻿using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.WebSockets;

using EM.App.Handlers.Membership;

namespace EM.App.Areas.ws.Controllers {

    /// <summary>
    /// WebSocket Membership Controller
    /// </summary>
    [RoutePrefix("ws/ms")]
    public class MembershipSocketController : BaseWebSocketController {

        /// <summary>
        /// login action
        /// </summary>
        /// <returns></returns>
        [Route("login")]
        [HttpGet]
        public HttpResponseMessage Login() {

            return ConstructWebSocketResponse(LoginSocket);

        }

        private Task LoginSocket(AspNetWebSocketContext context) {

            var handler = new LoginSocketHandler();
            return handler.ProcessWebSocketRequestAsync(context);

        }

        /// <summary>
        /// Register Action
        /// </summary>
        /// <returns></returns>
        [Route("register")]
        [HttpGet]
        public HttpResponseMessage Register() {

            return ConstructWebSocketResponse(RegisterSocket);

        }

        private Task RegisterSocket(AspNetWebSocketContext context) {

            var handler = new RegisterSocketHandler();
            return handler.ProcessWebSocketRequestAsync(context);

        }

        /// <summary>
        /// Activate Membership Action
        /// </summary>
        /// <returns></returns>
        [Route("activate")]
        [HttpGet]
        public HttpResponseMessage Activate() {

            return ConstructWebSocketResponse(ActivateSocket);

        }

        private Task ActivateSocket(AspNetWebSocketContext context) {

            var handler = new ActivateSocketHandler();
            return handler.ProcessWebSocketRequestAsync(context);

        }

        /// <summary>
        /// Recover Lost Password Action
        /// </summary>
        /// <returns></returns>
        [Route("recover")]
        [HttpGet]
        public HttpResponseMessage Recover() {

            return ConstructWebSocketResponse(RecoverSocket);

        }

        private Task RecoverSocket(AspNetWebSocketContext context) {

            var handler = new RecoverSocketHandler();
            return handler.ProcessWebSocketRequestAsync(context);

        }

        /// <summary>
        /// Change Lost Password Action
        /// </summary>
        /// <returns></returns>
        [Route("repass")]
        [HttpGet]
        public HttpResponseMessage Repass() {

            return ConstructWebSocketResponse(RepassSocket);

        }

        private Task RepassSocket(AspNetWebSocketContext context) {

            var handler = new RepassSocketHandler();
            return handler.ProcessWebSocketRequestAsync(context);

        }

    }

}
