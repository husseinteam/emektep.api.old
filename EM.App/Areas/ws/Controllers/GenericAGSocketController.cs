﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.WebSockets;
using DM.Domain.Data;

using EM.App.Handlers.Generic;
using EM.Extension.Core;
using EM.Objects.DomainObjects.Soru;
using Microsoft.Web.WebSockets;

namespace EM.App.Areas.ws.Controllers {

    /// <summary>
    /// Generic Aggragate Controller
    /// </summary>
    [RoutePrefix("ws/agg")]
    public class GenericAGSocketController : BaseWebSocketController {

        /// <summary>
        /// q/sorular is a sample query for this Action
        /// </summary>
        /// <returns>Returns a websecket response</returns>
        [Route("{slug}")]
        [HttpGet]
        public HttpResponseMessage Get() {

            return ConstructWebSocketResponse(AGSocket);

        }

        private Task AGSocket(AspNetWebSocketContext context) {

            var slug = Request.GetRouteData().Values["slug"].ToString();
            var type = SBuildEngine.BuildType(typeof(Question).Assembly.GetName(), slug);
            if (type == null) {
                throw new KeyNotFoundException("Slug {0} Not Found".Scatter(slug));
            }
            var handler = Activator.CreateInstance(typeof(AGSocketHandler<>).MakeGenericType(type)) as WebSocketHandler;
            return handler.ProcessWebSocketRequestAsync(context);

        }

    }

}
