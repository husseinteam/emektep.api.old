﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.WebSockets;
using DM.Domain.Data;

using EM.App.Handlers.Generic;
using EM.Extension.Core;
using EM.Objects.DomainObjects.Soru;
using Microsoft.Web.WebSockets;

namespace EM.App.Areas.ws.Controllers {

    /// <summary>
    /// Generic Query Controller
    /// </summary>
    [RoutePrefix("ws/q")]
    public class GenericQSocketController : BaseWebSocketController {

        /// <summary>
        /// q/sorular is a sample query for this Action
        /// </summary>
        /// <returns>Returns a websecket response</returns>
        [Route("{slug}")]
        [HttpGet]
        public HttpResponseMessage Get() {

            return ConstructWebSocketResponse(QSocket);

        }

        private Task QSocket(AspNetWebSocketContext context) {

            var slug = Request.GetRouteData().Values["slug"].ToString();
            var type = SBuildEngine.BuildType(typeof(Question).Assembly.GetName(), slug);
            if (type == null) {
                throw new KeyNotFoundException("Slug {0} Not Found".Scatter(slug));
            }
            var handler = Activator.CreateInstance(typeof(QSocketHandler<>).MakeGenericType(type)) as WebSocketHandler;
            return handler.ProcessWebSocketRequestAsync(context);

        }

        /// <summary>
        /// Finds in slugged table by id
        /// </summary>
        /// <returns>returns sinle element web socket response</returns>
        [Route("f/{slug}/{id}")]
        [HttpGet]
        public HttpResponseMessage FindByID() {

            return ConstructWebSocketResponse(QFindByIDSocket);

        }

        private Task QFindByIDSocket(AspNetWebSocketContext context) {

            var slug = Request.GetRouteData().Values["slug"].ToString();
            var type = SBuildEngine.BuildType(typeof(Question).Assembly.GetName(), slug);
            if (type == null) {
                throw new KeyNotFoundException("Slug {0} Not Found".Scatter(slug));
            }
            var id = Request.GetRouteData().Values["id"].ToInt();
            var handler = Activator.CreateInstance(
                typeof(QSocketHandler<>).MakeGenericType(type), id) as WebSocketHandler;
            return handler.ProcessWebSocketRequestAsync(context);

        }

    }

}
