﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.WebSockets;

namespace EM.App.Areas.ws.Controllers {

    public class BaseWebSocketController : ApiController {

        protected HttpResponseMessage ConstructWebSocketResponse(Func<AspNetWebSocketContext, Task> socketHandler) {

            if (HttpContext.Current.IsWebSocketRequest) {
                HttpContext.Current.AcceptWebSocketRequest(socketHandler);
                return Request.CreateResponse(HttpStatusCode.SwitchingProtocols);
            } else {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, 
                    new Exception("Not a websocket request"));
            }

        }

    }
}