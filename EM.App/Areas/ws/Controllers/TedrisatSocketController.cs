﻿using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.WebSockets;

using EM.App.Handlers.Tedrisat;

namespace EM.App.Areas.ws.Controllers {

    /// <summary>
    /// DersListesi View Fonksiyonlarını İhtiva Eder
    /// </summary>
    [RoutePrefix("ws/tedrisat")]
    public class TedrisatSocketController : BaseWebSocketController {


        /// <summary>
        /// Ogretim Uyesine Ait Dersler
        /// </summary>
        /// <returns>WebSocket Cevabı Döndürür</returns>
        [Route("dersler")]
        [HttpGet]
        public HttpResponseMessage OgretimUyesineAitDersler() {

            return ConstructWebSocketResponse(DersListesiSocket);

        }

        private Task DersListesiSocket(AspNetWebSocketContext context) {

            return new DerslerSocketHandler().ProcessWebSocketRequestAsync(context);

        }

        /// <summary>
        /// Derse Ait Öğrenciler
        /// </summary>
        /// <returns>WebSocket Cevabı Döndürür</returns>
        [Route("ogrenciler/{dersID}")]
        [HttpGet]
        public HttpResponseMessage DerseAitOgrenciler() {

            return ConstructWebSocketResponse(OgrencilerSocket);

        }

        private Task OgrencilerSocket(AspNetWebSocketContext context) {

            var dersID = Request.GetRouteData().Values["dersID"].ToString();
            return new OgrencilerSocketHandler(int.Parse(dersID)).ProcessWebSocketRequestAsync(context);

        }

    }
}
