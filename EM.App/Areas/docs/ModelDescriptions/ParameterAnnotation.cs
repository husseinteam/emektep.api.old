using System;

namespace EM.App.Areas.docs.ModelDescriptions {
    public class ParameterAnnotation
    {
        public Attribute AnnotationAttribute { get; set; }

        public string Documentation { get; set; }
    }
}