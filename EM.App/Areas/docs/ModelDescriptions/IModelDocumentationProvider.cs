using System;
using System.Reflection;

namespace EM.App.Areas.docs.ModelDescriptions {
    public interface IModelDocumentationProvider
    {
        string GetDocumentation(MemberInfo member);

        string GetDocumentation(Type type);
    }
}