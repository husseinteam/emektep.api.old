namespace EM.App.Areas.docs.ModelDescriptions {
    public class CollectionModelDescription : ModelDescription
    {
        public ModelDescription ElementDescription { get; set; }
    }
}