namespace EM.App.Areas.docs.ModelDescriptions {
    public class EnumValueDescription
    {
        public string Documentation { get; set; }

        public string Name { get; set; }

        public string Value { get; set; }
    }
}