﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.WebSockets;
using DM.Domain.Data;

using EM.App.Handlers.Generic;
using EM.Entities.VectorObjects;
using EM.Extension.Core;
using EM.Objects.DomainObjects.Soru;
using Microsoft.Web.WebSockets;
using Newtonsoft.Json.Linq;

namespace EM.App.Areas.api.Controllers {

    /// <summary>
    /// Generic Query Controller
    /// </summary>
    [RoutePrefix("api/q")]
    public class GenericQApiController : BaseApiController {

        /// <summary>
        /// q/sorular is a sample query for this Action
        /// </summary>
        /// <param name="json">Oturum Anahtarını İhtiva Eder</param>
        /// <returns>ActionResult İçinde Listeyi Döndürür</returns>
        [Route("{slug}")]
        [HttpPost]
        public IHttpActionResult Get(JObject json) {

            var slug = Request.GetRouteData().Values["slug"].ToString();
            var type = SBuildEngine.BuildType(typeof(Question).Assembly.GetName(), slug);
            if (type == null) {
                throw new KeyNotFoundException("Slug {0} Not Found".Scatter(slug));
            }
            var handler = Activator.CreateInstance(
                typeof(QApiHandler<>).MakeGenericType(type)) as IQSelector;

            IHttpActionResult result = null;
            Authorize(json.ToString(), (cursor) => {
                var vectors = new List<MessageVector>();
                result = Json(handler.SelectAll());
            }, (mvect) => {
                result = Json(mvect);
            });
            return result;
        }

        /// <summary>
        /// ID'si belirli elemanı bulur
        /// </summary>
        /// <param name="json">Oturum Anahtarını İhtiva Eder</param>
        /// <returns>ActionResult İçinde Elemanı Döndürür</returns>
        [Route("{slug}/the/{id}")]
        [HttpPost]
        public IHttpActionResult FindByID(JObject json) {

            var slug = Request.GetRouteData().Values["slug"].ToString();
            var type = SBuildEngine.BuildType(typeof(Question).Assembly.GetName(), slug);
            if (type == null) {
                throw new KeyNotFoundException("Slug {0} Not Found".Scatter(slug));
            }
            var id = Request.GetRouteData().Values["id"].ToInt();
            var handler = Activator.CreateInstance(
                typeof(QApiHandler<>).MakeGenericType(type)) as IQSelector;

            IHttpActionResult result = null;
            Authorize(json.ToString(), (cursor) => {
                var vectors = new List<MessageVector>();
                result = Json(handler.SelectThe(id));
            }, (mvect) => {
                result = Json(mvect);
            });
            return result;
        }

    }

}
