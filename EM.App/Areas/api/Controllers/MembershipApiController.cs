﻿using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.WebSockets;

using EM.App.Handlers.Membership;
using EM.App.Messengers.Membership;
using EM.Entities.VectorObjects;
using Newtonsoft.Json.Linq;

namespace EM.App.Areas.api.Controllers {

    /// <summary>
    /// Api Membership Controller
    /// </summary>
    [RoutePrefix("api/ms")]
    public class MembershipApiController : ApiController {

        /// <summary>
        /// Login Action
        /// </summary>
        /// <param name="json">Logon Parameter Wrapper</param>
        /// <returns>IHttpActionResult</returns>
        [Route("login")]
        [HttpPost]
        public IHttpActionResult Login(JObject json) {

            MessageVector vect = null; 
            LoginMessenger.Generate(json.ToString(), (msg) => vect = msg);
            return Ok(vect);

        }

        /// <summary>
        /// Register New User
        /// </summary>
        /// <param name="json">Register Parameter Wrapper</param>
        /// <returns>IHttpActionResult</returns>
        [Route("register")]
        [HttpPost]
        public IHttpActionResult Register(JObject json) {

            MessageVector vect = null;
            RegisterMessenger.Generate(json.ToString(), (msg) => vect = msg);
            return Ok(vect);

        }

        /// <summary>
        /// Activate Registered User
        /// </summary>
        /// <param name="json">Activate Parameter Wrapper</param>
        /// <returns>IHttpActionResult</returns>
        [Route("activate")]
        [HttpPost]
        public IHttpActionResult Activate(JObject json) {

            MessageVector vect = null;
            ActivateMessenger.Generate(json.ToString(), (msg) => vect = msg);
            return Ok(vect);

        }

        /// <summary>
        /// Recover Activated User
        /// </summary>
        /// <param name="json">Recover Parameter Wrapper</param>
        /// <returns>IHttpActionResult</returns>
        [Route("recover")]
        [HttpPost]
        public IHttpActionResult Recover(JObject json) {

            MessageVector vect = null;
            RecoverMessenger.Generate(json.ToString(), (msg) => vect = msg);
            return Ok(vect);

        }

        /// <summary>
        /// Reactivate activated user's password
        /// </summary>
        /// <param name="json">Repass Parameter Wrapper</param>
        /// <returns>IHttpActionResult</returns>
        [Route("repass")]
        [HttpPost]
        public IHttpActionResult Repass(JObject json) {

            MessageVector vect = null;
            RepassMessenger.Generate(json.ToString(), (msg) => vect = msg);
            return Ok(vect);

        }

    }

}
