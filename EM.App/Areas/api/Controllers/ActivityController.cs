﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.WebSockets;
using DM.Domain.Aggregate;
using EM.App.Handlers.Tedrisat;
using EM.App.Messengers.Landing;
using EM.App.Messengers.Membership;
using EM.App.Messengers.Tedrisat;
using EM.Entities.DomainObjects.Karsilama;
using EM.Entities.VectorObjects;
using EM.Extension.Core;
using Newtonsoft.Json.Linq;

namespace EM.App.Areas.api.Controllers {

    /// <summary>
    /// Anket Api Fonksiyonlarını İhtiva Eder
    /// </summary>
    [RoutePrefix("api/activity")]
    public class ActivityController : BaseApiController {

        /// <summary>
        /// Questions By Exam
        /// </summary>
        /// <param name="json">Token Vector Wrapper</param>
        /// <returns>IHttpActionResult Content With Questions</returns>
        [Route("questionsof/{examID}")]
        [HttpPost]
        public IHttpActionResult QuestionsOfExam(JObject json) {

            var examID = int.Parse(Request.GetRouteData().Values["examID"].ToString());

            IHttpActionResult result = null;
            Authorize(json.ToString(), (cursor) => {
                List<MessageVector> vectors = new List<MessageVector>();
                ActivitiesMessenger.GenerateQuestions(examID, cursor,
              mvect => vectors.Add(mvect),
              () => {
                  result = Json(new MessageVector() {
                      done = true,
                      data = vectors.Select(v => v.data)
                  });
              },
              () => {
                  result = Json(new MessageVector() {
                      done = false,
                      messages = new[] { "Veri Yok" }
                  });
              });
            }, (mvect) => {
                result = Json(mvect);
            });
            return result;
        }

        /// <summary>
        /// Choices By Questions
        /// </summary>
        /// <param name="json">Token Vector Wrapper</param>
        /// <returns>IHttpActionResult Content With TextChoices</returns>
        [Route("choicesof/{questionID}")]
        [HttpPost]
        public IHttpActionResult ChoicesOfQuestions(JObject json) {

            var questionID = int.Parse(Request.GetRouteData()
                .Values["questionID"].ToString());

            IHttpActionResult result = null;
            Authorize(json.ToString(), (cursor) => {
                List<MessageVector> vectors = new List<MessageVector>();
                ActivitiesMessenger.GenerateTestChoices(questionID, cursor,
              mvect => vectors.Add(mvect),
              () => {
                  result = Json(new MessageVector() {
                      done = true,
                      data = vectors.Select(v => v.data)
                  });
              },
              () => {
                  result = Json(new MessageVector() {
                      done = false,
                      messages = new[] { "Veri Yok" }
                  });
              });
            }, (mvect) => {
                result = Json(mvect);
            });
            return result;
        }


    }
}
