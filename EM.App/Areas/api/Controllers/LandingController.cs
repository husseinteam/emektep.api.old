﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.WebSockets;
using DM.Domain.Aggregate;
using EM.App.Handlers.Tedrisat;
using EM.App.Messengers.Landing;
using EM.App.Messengers.Membership;
using EM.App.Messengers.Tedrisat;
using EM.Entities.DomainObjects.Karsilama;
using EM.Entities.VectorObjects;
using EM.Extension.Core;
using Newtonsoft.Json.Linq;

namespace EM.App.Areas.api.Controllers {

    /// <summary>
    /// Karşılama Sayfası Api Fonksiyonlarını İhtiva Eder
    /// </summary>
    [RoutePrefix("api/karsilama")]
    public class LandingController : BaseApiController {

        /// <summary>
        /// AkademikTakvim i Ihtiva Eder
        /// </summary>
        /// <param name="json">Oturum Anahtarını İhtiva Eder</param>
        /// <returns>ActionResult İçinde Akademik Takvimi Döndürür</returns>
        [Route("akademik-takvim")]
        [HttpPost]
        public IHttpActionResult AkademikTakvim(JObject json) {

            IHttpActionResult result = null;
            Authorize(json.ToString(), (cursor) => {
                List<MessageVector> vectors = new List<MessageVector>();
                AkademikTakvimMessenger.Generate(cursor,
                  mvect => vectors.Add(mvect),
                  () => {
                      result = Json(new MessageVector() {
                          done = true,
                          data = vectors.Select(v => v.data)
                      });
                  }, 
              () => {
                  result = Json(new MessageVector() {
                      done = false,
                      messages = new[] { "Veri Yok" }
                  });
              });
            }, (mvect) => {
                result = Json(mvect);
            });
            return result;

        }

        /// <summary>
        /// Duyuruları İşler Api'dır
        /// </summary>
        /// <param name="json">Oturum Anahtarını İhtiva Eder</param>
        /// <returns>ActionResult İçinde Duyuruları Döndürür</returns>
        [Route("duyurular")]
        [HttpPost]
        public IHttpActionResult Duyuru(JObject json) {
            return Bulletins(json, EBulletinType.Announcement);

        }

        /// <summary>
        /// Haberleri İşler Api'dır
        /// </summary>
        /// <param name="json">Oturum Anahtarını İhtiva Eder</param>
        /// <returns>ActionResult İçinde Haberleri Döndürür</returns>
        [Route("haberler")]
        [HttpPost]
        public IHttpActionResult Haberler(JObject json) {
            return Bulletins(json, EBulletinType.News);

        }

        /// <summary>
        /// Aktiviteleri İşler Api'dır
        /// </summary>
        /// <param name="json">Oturum Anahtarını İhtiva Eder</param>
        /// <returns>ActionResult İçinde Aktiviteleri Döndürür</returns>
        [Route("aktiviteler")]
        [HttpPost]
        public IHttpActionResult Aktiviteler(JObject json) {
            return Bulletins(json, EBulletinType.Activity);

        }

        private IHttpActionResult Bulletins(JObject json, EBulletinType type) {
            IHttpActionResult result = null;
            Authorize(json.ToString(), (cursor) => {
                List<MessageVector> vectors = new List<MessageVector>();
                BulletinMessenger.Generate(type, cursor,
                  mvect => vectors.Add(mvect),
                  () => {
                      result = Json(new MessageVector() {
                          done = true,
                          data = vectors.Select(v => v.data)
                      });
                  },
              () => {
                  result = Json(new MessageVector() {
                      done = false,
                      messages = new[] { "Veri Yok" }
                  });
              });
            }, (mvect) => {
                result = Json(mvect);
            });
            return result;
        }
    }
}
