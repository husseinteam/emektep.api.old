﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.WebSockets;
using DM.Domain.Aggregate;
using DM.Domain.DML;
using DM.Domain.Query;
using EM.App.Handlers.Tedrisat;
using EM.App.Messengers.Membership;
using EM.Entities.AggragateObjects.Tedrisat;
using EM.Entities.DomainObjects.AkademikKadro;
using EM.Entities.DomainObjects.Uyelik;
using EM.Entities.VectorObjects;
using EM.Extension.Core;
using EM.Objects.DomainObjects.AkademikKadro;
using EM.Objects.DomainObjects.Kurum;
using EM.Objects.DomainObjects.Mufredat;
using Newtonsoft.Json.Linq;

namespace EM.App.Areas.api.Controllers {

    /// <summary>
    /// DersListesi View Fonksiyonlarını İhtiva Eder
    /// </summary>
    [RoutePrefix("api/tedrisat")]
    public class TedrisatApiController : BaseApiController {

        /// <summary>
        /// Inserts Student
        /// </summary>
        /// <param name="json">Is a OgrenciListesiAG plus token</param>
        /// <returns>MVect wrapped into a IHttpActionResult</returns>
        [Route("ekle/ogrenci")]
        [HttpPost]
        public IHttpActionResult OgrenciEkle(JObject json) {

            IHttpActionResult result = null;
            Authorize(json.ToString(), (cursor) => {
                var ogrli = json.ToString().FromJson<OgrenciListesiAG>();
                CR<Ogrenci>.CRUD(new Ogrenci() {
                    KayitlanmaTipi = ogrli.OgrenciKayitTipi,
                    KayitYili = ogrli.OgrenciKayitYili,
                    OgrenciNo = ogrli.OgrenciNo,
                    UyeID = ogrli.UyeID
                }).Upsert(ders => {
                    result = Json(new MessageVector() {
                        done = true,
                        data = ogrli,
                        messages = new[] { "İşlem Başarıldı" }
                    });
                }, exc => {
                    result = Json(new MessageVector() {
                        done = false,
                        messages = new[] { exc.Message }
                    });
                });
            }, (mvect) => {
                result = Json(mvect);
            });
            return result;

        }

        /// <summary>
        /// Updates Student
        /// </summary>
        /// <param name="json">Is a OgrenciListesiAG plus token</param>
        /// <returns>MVect wrapped into a IHttpActionResult</returns>
        [Route("duzenle/ogrenci/{uyeID}")]
        [HttpPost]
        public IHttpActionResult OgrenciDuzenle(JObject json) {

            IHttpActionResult result = null;
            var uyeID = int.Parse(Request.GetRouteData().Values["uyeID"].ToString());
            Authorize(json.ToString(), (cursor) => {
                var ogrli = json.ToString().FromJson<OgrenciListesiAG>();
                ogrli.UyeID = uyeID;
                Q<Ogrenci>.SelectAllColumns().Where(d => d.UyeID == uyeID).ExecuteOne(deq => {
                    deq.Modify()
                    .Update(d => d.KayitlanmaTipi).Set(ogrli.OgrenciKayitTipi)
                    .Update(d => d.KayitYili).Set(ogrli.OgrenciKayitYili)
                    .Update(d => d.OgrenciNo).Set(ogrli.OgrenciNo)
                    .PersistUpdate((rowc, ders) => {
                        Q<Uye>.SelectAllColumns().Where(u => u.ID == ders.UyeID)
                            .ExecuteOne(ucur => {
                                ucur.Modify().Update(u => u.FullName).Set(ogrli.OgrenciAdiSoyadi)
                                    .PersistUpdate((rowcc, uye) => {
                                        result = Json(new MessageVector() {
                                            done = true,
                                            data = ogrli
                                        });
                                    });
                            }, () => {
                                result = Json(new MessageVector() {
                                    done = false,
                                    messages = new[] { "Hata Oluştu" }
                                });
                            });
                    });
                }, () => {
                    result = Json(new MessageVector() {
                        done = false,
                        messages = new[] { "Hata Oluştu" }
                    });
                });
            }, (mvect) => {
                result = Json(mvect);
            });
            return result;
        }

        /// <summary>
        /// Inserts Lecture
        /// </summary>
        /// <param name="json">Is a DersListesiAG plus token</param>
        /// <returns>MVect wrapped into a IHttpActionResult</returns>
        [Route("ekle/ders")]
        [HttpPost]
        public IHttpActionResult DersEkle(JObject json) {

            IHttpActionResult result = null;
            Authorize(json.ToString(), (cursor) => {
                var dlag = json.ToString().FromJson<DersListesiAG>();
                CR<Ders>.CRUD(new Ders() {
                    DersAdi = dlag.DersAdi,
                    DersKodu = dlag.DersKodu,
                    Kredi = dlag.Kredi,
                    TeorikSaat = dlag.TeorikSaat,
                    UygulamaSaat = dlag.UygulamaSaat,
                    DonemID = dlag.DonemID
                }).Upsert(ders => {
                    result = Json(new MessageVector() {
                        done = true,
                        data = dlag,
                        messages = new[] { "İşlem Başarıldı" }
                    });
                }, exc => {
                    result = Json(new MessageVector() {
                        done = false,
                        messages = new[] { exc.Message }
                    });
            });
            }, (mvect) => {
                result = Json(mvect);
            });
            return result;

        }

        /// <summary>
        /// Updates Lecture
        /// </summary>
        /// <param name="json">Is a DersListesiAG plus token</param>
        /// <returns>MVect wrapped into a IHttpActionResult</returns>
        [Route("duzenle/ders/{dersID}")]
        [HttpPost]
        public IHttpActionResult DersDuzenle(JObject json) {

            IHttpActionResult result = null;
            var dersID = int.Parse(Request.GetRouteData().Values["dersID"].ToString());
            Authorize(json.ToString(), (cursor) => {
                var dlag = json.ToString().FromJson<DersListesiAG>();
                dlag.DersID = dersID;
                Q<Ders>.SelectAllColumns().Where(d => d.ID == dersID).ExecuteOne(deq => {
                    deq.Modify()
                    .Update(d => d.DersAdi).Set(dlag.DersAdi)
                    .Update(d => d.DersKodu).Set(dlag.DersKodu)
                    .Update(d => d.TeorikSaat).Set(dlag.TeorikSaat)
                    .Update(d => d.UygulamaSaat).Set(dlag.UygulamaSaat)
                    .Update(d => d.Kredi).Set(dlag.Kredi)
                    .PersistUpdate((rowc, ders) => {
                        result = Json(new MessageVector() {
                            done = true,
                            data = dlag
                        });
                    });
                }, () => {
                    result = Json(new MessageVector() {
                        done = false,
                        messages = new[] { "Hata Oluştu" }
                    });
                });
            }, (mvect) => {
                result = Json(mvect);
            });
            return result;
        }

        /// <summary>
        /// Lectures By Current Member
        /// </summary>
        /// <param name="json">Token Vector Wrapper</param>
        /// <returns>IHttpActionResult Content With Lectures</returns>
        [Route("dersler")]
        [HttpPost]
        public IHttpActionResult OgretimUyesineAitDersler(JObject json) {

            IHttpActionResult result = null;
            Authorize(json.ToString(), (cursor) => {
                List<MessageVector> vectors = new List<MessageVector>();
                DerslerMessenger.Generate(cursor,
              mvect => vectors.Add(mvect),
              () => {
                  result = Json(new MessageVector() {
                      done = true,
                      data = vectors.Select(v => v.data)
                  });
              },
              () => {
                  result = Json(new MessageVector() {
                      done = false,
                      messages = new[] { "Veri Yok" }
                  });
              });
            }, (mvect) => {
                result = Json(mvect);
            });
            return result;

        }

        /// <summary>
        /// Students By Lecture
        /// </summary>
        /// <param name="json">Token Vector Wrapper</param>
        /// <returns>IHttpActionResult Content With Students</returns>
        [Route("ogrenciler/{dersID}")]
        [HttpPost]
        public IHttpActionResult DerseAitOgrenciler(JObject json) {

            var dersID = int.Parse(Request.GetRouteData().Values["dersID"].ToString());

            IHttpActionResult result = null;
            Authorize(json.ToString(), (cursor) => {
                List<MessageVector> vectors = new List<MessageVector>();
                OgrencilerMessenger.Generate(dersID, cursor,
              mvect => vectors.Add(mvect),
              () => {
                  result = Json(new MessageVector() {
                      done = true,
                      data = vectors.Select(v => v.data)
                  });
              },
              () => {
                  result = Json(new MessageVector() {
                      done = false,
                      messages = new[] { "Veri Yok" }
                  });
              });
            }, (mvect) => {
                result = Json(mvect);
            });
            return result;

        }

    }
}
