﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Description;
using EM.App.HttpTools;
using EM.Extension.Core;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;

namespace EM.App.Areas.api.Controllers.NonSocketControllers {

    /// <summary>
    /// Export To Excel Word Pdf
    /// </summary>
    [RoutePrefix("api/export")]
    public class ExportApiController : ApiController {

        /// <summary>
        /// Exports Incoming data to Excel Data
        /// </summary>
        /// <param name="dict">Incoming JToken table</param>
        /// <returns>Excel File Url</returns>
        [Route("toexcel")]
        [HttpPost]
        public IHttpActionResult ExportToExcel(JToken dict) {

            return SHttpTools.GenerateExcelDocument(dict, exc => {
                return Json(new {
                    url = exc.FileUrl,
                    name = exc.FileName
                });
            }, (exc) => {
                return BadRequest("Exception: {0}".Scatter(exc.Message)) as IHttpActionResult;
            });

        }

        /// <summary>
        /// Exports Incoming data to Excel Data
        /// </summary>
        /// <param name="dict">Incoming JToken table</param>
        /// <returns>Pdf File Url</returns>
        [Route("topdf")]
        [HttpPost]
        public IHttpActionResult ExportToPdf(JToken dict) {

            return SHttpTools.GeneratePdfDocument(dict, pdf => {
                return Json(new {
                    url = pdf.FileUrl,
                    name = pdf.FileName
                });
            }, exc => {
                return BadRequest("Exception: {0}".Scatter(exc.Message)) as IHttpActionResult;
            });
            ;

        }
        /// <summary>
        /// Exports Incoming data to Word Data
        /// </summary>
        /// <param name="dict">Incoming JToken table</param>
        /// <returns>Word File Url</returns>
        [Route("toword")]
        [HttpPost]
        public IHttpActionResult ExportToWord(JToken dict) {

            return SHttpTools.GenerateWordDocument(dict, pdf => {
                return Json(new {
                    url = pdf.FileUrl,
                    name = pdf.FileName
                });
            }, exc => {
                return BadRequest("Exception: {0}".Scatter(exc.Message)) as IHttpActionResult;
            });
            ;

        }

    }

}
