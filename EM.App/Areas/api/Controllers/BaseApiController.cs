﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DM.Domain.Query;
using EM.Entities.DomainObjects.Uyelik;
using EM.Entities.VectorObjects;
using EM.Extension.Core;

namespace EM.App.Areas.api.Controllers {

    public class BaseApiController : ApiController {

        protected void Authorize(string message, Action<IExecutedQuery<Uye>> uyeeq, Action<MessageVector> failback) {
            
            var mvect = message.FromJson<TokenVector>();
            if (mvect == null || mvect.token == null) {
                failback(new MessageVector() {
                    done = false,
                    token = "",
                    messages = new[] { "Oturum Açmış Olmanız Gerekiyor." }
                });
                return;
            }
            Q<Uye>.SelectAllColumns().Where(u => u.AccessToken == mvect.token)
                .ExecuteOne(cursor => {
                    if (DateTime.Now < cursor.ResolvedEntity.AccessTokenExpiresOn) {
                        uyeeq(cursor);
                    } else {
                        cursor.Modify()
                           .Update(x => x.AccessTokenExpiresOn).Set(DateTime.MinValue)
                           .Update(x => x.AccessToken).Set("").PersistUpdate((rowc, entity) => {
                               failback(new MessageVector() {
                                   done = false,
                                   token = "",
                                   messages = new[] { "Yeniden Giriş Yapmalısınız." }
                               });
                           });
                    }
                }, () => {
                    failback(new MessageVector() {
                        done = false,
                        token = "",
                        messages = new[] { "Oturum Anahtarı Geçersiz." }
                    });
                });

        }

    }

}
