﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DM.Domain.Aggregate;
using DM.Domain.Query;
using EM.Entities.AggragateObjects.Tedrisat;
using EM.Entities.DomainObjects.Uyelik;
using EM.Entities.VectorObjects;
using EM.Extension.Core;
using Newtonsoft.Json;

namespace EM.App.Messengers.Membership {

    internal class DerslerMessenger {

        internal static void Generate(IExecutedQuery<Uye> cursor, Action<MessageVector> messenger, Action lastback, Action noneback) {

            var uye = cursor.ResolvedEntity;
            AG<DersListesiAG>.Views().SetDistinctColumn(x => x.UyeID)
                .SelectAllColumns().Where(ou => ou.UyeID == uye.ID).ExecuteList(c => {
                    messenger(new MessageVector() {
                        done = true,
                        data = c
                    });
                }, noneback);
            lastback();

        }

    }
}