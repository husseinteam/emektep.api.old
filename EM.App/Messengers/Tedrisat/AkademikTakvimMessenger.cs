﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DM.Domain.Aggregate;
using DM.Domain.Query;
using EM.Entities.AggragateObjects.AkademikKadro;
using EM.Entities.DomainObjects.Uyelik;
using EM.Entities.VectorObjects;
using EM.Extension.Core;
using Newtonsoft.Json;

namespace EM.App.Messengers.Tedrisat {

    internal class AkademikTakvimMessenger {

        internal static void Generate(IExecutedQuery<Uye> cursor, Action<MessageVector> messenger, Action lastback, Action noneback) {

            var uye = cursor.ResolvedEntity;
            AG<AkademikTakvimAG>.Views()
                .SelectAllColumns().ExecuteList(c => {
                    messenger(new MessageVector() {
                        done = true,
                        data = c
                    });
                }, noneback);
            lastback();
        }

    }
}