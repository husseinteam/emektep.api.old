﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DM.Domain.Aggregate;
using DM.Domain.Query;
using EM.Entities.AggragateObjects.Tedrisat;
using EM.Entities.DomainObjects.Uyelik;
using EM.Entities.VectorObjects;
using EM.Extension.Core;
using EM.Objects.DomainObjects.Soru;
using Newtonsoft.Json;

namespace EM.App.Messengers.Membership {

    internal class ActivitiesMessenger {

        internal static void GenerateQuestions(int examID, IExecutedQuery<Uye> cursor, Action<MessageVector> messenger, Action lastback, Action noneback) {

            var uye = cursor.ResolvedEntity;
            Q<Question>.SelectAllColumns().Where(ou => ou.ExamID == examID).ExecuteMany(c => {
                messenger(new MessageVector() {
                    done = true,
                    data = c.ResolvedEntity
                });
            }, noneback);
            lastback();
        }

        internal static void GenerateTestChoices(int questionID, IExecutedQuery<Uye> cursor, Action<MessageVector> messenger, Action lastback, Action noneback) {

            var uye = cursor.ResolvedEntity;
            Q<TestChoice>.SelectAllColumns().Where(ou => ou.QuestionID == questionID).ExecuteMany(c => {
                messenger(new MessageVector() {
                    done = true,
                    data = c.ResolvedEntity
                });
            }, noneback);
            lastback();
        }

    }
}