﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DM.Domain.Aggregate;
using DM.Domain.Query;
using EM.Entities.AggragateObjects.Tedrisat;
using EM.Entities.DomainObjects.Uyelik;
using EM.Entities.VectorObjects;
using EM.Extension.Core;
using Newtonsoft.Json;

namespace EM.App.Messengers.Membership {

    internal class OgrencilerMessenger {

        internal static void Generate(int dersID, IExecutedQuery<Uye> cursor, Action<MessageVector> messenger, Action lastback, Action noneback) {

            var uye = cursor.ResolvedEntity;
            AG<OgrenciListesiAG>.Views().SetDistinctColumn(x => x.DersID).SelectAllColumns().Where(ou => ou.OgretimUyesiUyeID == uye.ID && ou.DersID == dersID).ExecuteList(c => {
                messenger(new MessageVector() {
                    done = true,
                    data = c
                });
            }, noneback);
            lastback();

        }

    }
}