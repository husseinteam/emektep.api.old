﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Web;
using DM.Domain.DML;
using DM.Domain.Query;
using EM.App.HttpTools;
using EM.Entities.DomainObjects.Uyelik;
using EM.Entities.VectorObjects;
using EM.Extension.Core;
using EM.Service.Container;
using EM.Service.Contract;
using EM.Service.Enum;
using Newtonsoft.Json;

namespace EM.App.Messengers.Membership {

    internal class RegisterMessenger {

        internal static void Generate(string json, Action<MessageVector> messenger) {

            var registering = JsonConvert.DeserializeObject<Uye>(json);
            registering.ActivationToken = 2.GenerateToken();
            registering.ActivationTokenExpiresOn = DateTime.Now.AddMinutes(30);
            registering.Password = registering.Password.GenerateMD5();
            Q<Uye>.SelectAllColumns().Where(u => u.Contact == registering.Contact).ExecuteOne(uye => {
                messenger(new MessageVector() {
                    done = false,
                    messages = new string[] {
                    "{0} ile Kayıtlı Bir Kullanıcı Zaten Mevcut"
                        .Scatter(uye.ResolvedEntity.Contact) }
                });
            }, () => {
                CR<Uye>.CRUD(registering).Upsert(
                    entity => {
                        ConfigurationManager.AppSettings["ActivationLink"] = HttpContext.Current.Request.AddHttpPrefix(
                            "#uyelik/aktivasyon/{0}".Scatter(entity.ActivationToken));
                        SServiceContainer.Resolve<IMailService>().SendMail(
                            ConfigurationManager.AppSettings,
                            new MailAddressCollection().Append(
                                new MailAddress(entity.Contact)) as MailAddressCollection,
                            EMailType.ActivationMail, () => messenger(new MessageVector() {
                                done = true,
                                messages = new string[] { "Operation Succeeded" }
                            }),
                            (exc) => messenger(new MessageVector() {
                                done = false,
                                messages = new string[] { "Kritik Hata", exc.Message }
                            }));

                    },
                    exc => {
                        messenger(new MessageVector() {
                            done = false,
                            messages = new string[] { "Kritik Hata", exc.Message }
                        });
                    }
                );
            });

        }

    }
}