﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Web;
using DM.Domain.Query;
using EM.App.HttpTools;
using EM.Entities.DomainObjects.Uyelik;
using EM.Entities.VectorObjects;
using EM.Extension.Core;
using EM.Service.Container;
using EM.Service.Contract;
using EM.Service.Enum;
using Newtonsoft.Json;

namespace EM.App.Messengers.Membership {

    internal class RecoverMessenger {

        internal static void Generate(string json, Action<MessageVector> messenger) {

            var registering = JsonConvert.DeserializeObject<Uye>(json);
            Q<Uye>.SelectAllColumns().Where(u => u.Contact == registering.Contact).ExecuteOne(uye => {
                if (string.IsNullOrEmpty(uye.ResolvedEntity.TCKN)) {
                    Q<Uye>.SelectAllColumns().Where(x => x.ID == uye.ResolvedEntity.ID)
                        .ExecuteOne(xq => {
                            var deleted = xq.Delete();
                            if (deleted) {
                                messenger(new MessageVector() {
                                    done = false,
                                    messages = new string[] { "Üyeliğiniz Aktive Edilmemiş. Kaydınız Silinmiştir. Lütfen Yeniden Üye Olunuz" }
                                });
                            } else {
                                messenger(new MessageVector() {
                                    done = false,
                                    messages = new string[] { "Üyeliğinizin İptali Sırasında Kritik Hata" }
                                });
                            }
                        }, () => messenger(new MessageVector() {
                            done = false,
                            messages = new string[] { "Kullanıcı Bulunamadı" }
                        })
                    );
                } else {
                    uye.Modify()
                    .Update(u => u.RecoveryTokenExpiresOn).Set(DateTime.Now.AddMinutes(45))
                    .Update(u => u.RecoveryToken).Set(2.GenerateToken())
                .PersistUpdate((rowc, entity) => {
                    ConfigurationManager.AppSettings["RecoverLink"] = HttpContext.Current.Request.AddHttpPrefix(
                           "uyelik/sifremiunuttum/{0}".Scatter(entity.RecoveryToken));
                    SServiceContainer.Resolve<IMailService>().SendMail(
                        ConfigurationManager.AppSettings,
                        new MailAddressCollection().Append(
                            new MailAddress(entity.Contact)) as MailAddressCollection,
                        EMailType.RecoverMail, () =>
                        messenger(new MessageVector() {
                            done = true,
                            messages = new string[] { "Kayıtlarımızda Mevcutsa Belirttiğiniz Adrese Bir E-Posta Gönderilecektir" }
                        }),
                        (exc) => messenger(new MessageVector() {
                            done = false,
                            messages = new string[] { "E-Posta Gönderilirken Hata Oluştu", exc.Message }
                        }));
                });
                }
            }, () => {
                messenger(new MessageVector() {
                    done = true,
                    messages = new string[] { "Kayıtlarımızda Mevcutsa Belirttiğiniz Adrese Bir E-Posta Gönderilecektir" }
                });
            });

        }

    }
}