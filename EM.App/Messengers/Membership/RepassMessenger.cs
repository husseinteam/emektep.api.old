﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Web;
using DM.Domain.DML;
using DM.Domain.Query;
using EM.App.HttpTools;
using EM.Entities.DomainObjects.Uyelik;
using EM.Entities.VectorObjects;
using EM.Extension.Core;
using EM.Service.Container;
using EM.Service.Contract;
using EM.Service.Enum;
using Newtonsoft.Json;

namespace EM.App.Messengers.Membership {

    internal class RepassMessenger {

        internal static void Generate(string json, Action<MessageVector> messenger) {

            var activating = JsonConvert.DeserializeObject<Uye>(json);
            Q<Uye>.SelectAllColumns().Where(u => u.Contact == activating.Contact)
                .And(u => u.RecoveryToken == activating.RecoveryToken)
                .ExecuteOne(uye => {
                    if (DateTime.Now < uye.ResolvedEntity.RecoveryTokenExpiresOn) {
                        uye.Modify()
                            .Update(x => x.Password).Set(activating.Password.GenerateMD5())
                            .Update(x => x.RecoveryToken).Set("")
                            .Update(x => x.RecoveryTokenExpiresOn).Set(DateTime.MinValue)
                            .PersistUpdate((rowc, entity) => {
                                messenger(new MessageVector() {
                                    done = true,
                                    messages = new string[] {
                                "{0} Kullanıcısının Şifresi Başarıyla  Değiştirilmiştir"
                                .Scatter(entity.FullName) }
                                });
                            });
                    } else {
                        messenger(new MessageVector() {
                            done = false,
                            messages = new string[] { "Şifre Kurtarma Vadesi Dolmuştur, Lütfen İşlemi Tekrarlayınız" }
                        });
                    }
                }, () => {
                    messenger(new MessageVector() {
                        done = false,
                        messages = new string[] { "Geçersiz Anahtar" }
                    });
                });


        }

    }
}