﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DM.Domain.Query;
using EM.Entities.DomainObjects.Uyelik;
using EM.Entities.VectorObjects;
using EM.Extension.Core;
using Newtonsoft.Json;

namespace EM.App.Messengers.Membership {

    internal class LoginMessenger {

        internal static void Generate(string json, Action<MessageVector> messenger) {
            
            var lcred = JsonConvert.DeserializeObject<LogonCredential>(json);
            Q<Uye>.SelectAllColumns().Where(u => u.TCKN == lcred.Logon).And(u => u.Password == lcred.Password.GenerateMD5()).ExecuteOne(cursor => {
                cursor.Modify()
                        .Update(x => x.AccessTokenExpiresOn).Set(DateTime.Now.AddMinutes(17))
                        .Update(x => x.AccessToken).Set(1.GenerateToken()).PersistUpdate((rowc, entity) => {
                            messenger(new MessageVector() {
                                done = true,
                                token = entity.AccessToken,
                                data = entity.ID,
                                messages = new string[] {
                                        "Hoşgeldiniz {0}".Scatter(entity.FullName)
                                    }
                            });
                        });
            }, () => {
                messenger(new MessageVector() {
                    done = false,
                    messages = new[] { "Bilgiler Kayıtlarımızla Uyuşmuyor" }
                });
            });

        }

    }
}