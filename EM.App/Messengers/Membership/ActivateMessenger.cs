﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DM.Domain.Query;
using EM.Entities.DomainObjects.Uyelik;
using EM.Entities.VectorObjects;
using EM.Extension.Core;
using Newtonsoft.Json;

namespace EM.App.Messengers.Membership {

    internal class ActivateMessenger {

        internal static void Generate(string json, Action<MessageVector> messenger) {

            var activating = JsonConvert.DeserializeObject<Uye>(json);
            Q<Uye>.SelectAllColumns().Where(u => u.ActivationToken == activating.ActivationToken)
                .ExecuteOne(uye => {
                    if (DateTime.Now < uye.ResolvedEntity.ActivationTokenExpiresOn) {
                        if (uye.ResolvedEntity.Password == activating.Password.GenerateMD5()) {
                            uye.Modify()
                                .Update(x => x.TCKN).Set(activating.TCKN)
                                .Update(x => x.FullName).Set(activating.FullName)
                                .Update(x => x.Password).Set(activating.Password)
                                .Update(x => x.ActivationToken).Set("")
                                .Update(x => x.ActivationTokenExpiresOn).Set(DateTime.MinValue)
                                .PersistUpdate((rowc, entity) => {
                                    messenger(new MessageVector() {
                                        done = true,
                                        messages = new string[] {
                                    "{0} Kullanıcısına Ait Üye Kaydı Tamamlanmıştır"
                                        .Scatter(entity.FullName) }
                                    });
                                });
                        } else {
                            messenger(new MessageVector() {
                                done = false,
                                messages = new string[] { "Şifrenizi Doğru Girmediniz" }
                            });
                        }
                    } else {
                        Q<Uye>.SelectAllColumns().Where(x => x.ID == uye.ResolvedEntity.ID)
                        .ExecuteOne(xq => {
                            var deleted = xq.Delete();
                            if (deleted) {
                                messenger(new MessageVector() {
                                    done = false,
                                    messages = new string[] { "Aktivasyon Süreniz Dolmuştur, Lütfen İşlemi Tekrarlayınız" }
                                });
                            } else {
                                messenger(new MessageVector() {
                                    done = false,
                                    messages = new string[] { "Aktivasyon İptali Sırasında Kritik Hata" }
                                });
                            }
                        }, () => messenger(new MessageVector() {
                                done = false,
                                messages = new string[] { "Kullanıcı Bulunamadı" }
                            })
                        );
                    }

                }, () => {
                    messenger(new MessageVector() {
                        done = false,
                        messages = new string[] { "Geçersiz Anahtar" }
                    });
                });

        }

    }
}