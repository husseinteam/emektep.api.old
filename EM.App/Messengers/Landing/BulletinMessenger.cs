﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DM.Domain.Aggregate;
using DM.Domain.Query;
using EM.Entities.AggragateObjects.Karsilama;
using EM.Entities.DomainObjects.Karsilama;
using EM.Entities.DomainObjects.Uyelik;
using EM.Entities.VectorObjects;
using EM.Extension.Core;
using Newtonsoft.Json;

namespace EM.App.Messengers.Landing {

    internal class BulletinMessenger {

        internal static void Generate(EBulletinType tip, IExecutedQuery<Uye> cursor, Action<MessageVector> messenger, Action lastback, Action noneback) {

            var uye = cursor.ResolvedEntity;
            AG<BulletinAG>.Views()
                .SelectAllColumns().Where(bl => bl.Tip == tip).ExecuteList(c => {
                    messenger(new MessageVector() {
                        done = true,
                        data = c
                    });
                }, noneback);
            lastback();

        }

    }
}