﻿using System;
using System.Collections.Generic;
using DM.Domain.Query;
using EM.Entities.DomainObjects.Uyelik;
using EM.Entities.VectorObjects;
using EM.Extension.Core;
using Microsoft.Web.WebSockets;

namespace EM.App.Handlers.Base {

    class BaseMessenger : BaseMembershipSocketHandler {

        private Action<MessageVector> messenger;
        protected static List<WebSocketHandler> _Clients = new List<WebSocketHandler>();

        public BaseMessenger(Action<MessageVector> messenger) {
            this.messenger = messenger;
            Unauthorized += (message) => {
                messenger(new MessageVector() {
                    done = false,
                    messages = new[] { message }
                });
            };
        }

        public sealed override void OnMessage(string message) {

            var mvect = message.FromJson<TokenVector>();
            Q<Uye>.SelectAllColumns().Where(u => u.AccessToken == mvect.token)
                .ExecuteOne(cursor => {
                    if (DateTime.Now < cursor.ResolvedEntity.AccessTokenExpiresOn) {
                        Authorized(cursor);
                    } else {
                        cursor.Modify()
                           .Update(x => x.AccessTokenExpiresOn).Set(DateTime.MinValue)
                           .Update(x => x.AccessToken).Set("").PersistUpdate((rowc, entity) => {
                               this.messenger(new MessageVector() {
                                   done = false,
                                   token = "",
                                   messages = new[] { "Yeniden Giriş Yapmalısınız." }
                               });
                           });
                    }
                }, () => OnUnauthorized("Oturum Açmış Olmanız Gerekiyor."));
        }


        public override void OnOpen() {
            OnSocketOpened(_Clients);
        }

        public override void OnClose() {
            OnSocketClosed(_Clients);
        }

        protected delegate void AuthorizedCursorHandle(IExecutedQuery<Uye> cursor);
        protected delegate void UnauthorizedCursorHandle(string message);
        protected delegate void SocketOpenedHandle(List<WebSocketHandler> clients);
        protected delegate void SocketClosedHandle(List<WebSocketHandler> clients);

        protected event AuthorizedCursorHandle Authorized;
        protected event UnauthorizedCursorHandle Unauthorized;
        protected event SocketOpenedHandle SocketOpened;
        protected event SocketClosedHandle SocketClosed;

        protected void OnAuthorized(IExecutedQuery<Uye> cursor) {
            if (Authorized != null) {
                Authorized(cursor);
            }
        }
        protected void OnUnauthorized(string message) {
            if (Unauthorized != null) {
                Unauthorized(message);
            }
        }
        protected void OnSocketOpened(List<WebSocketHandler> clients) {
            if (SocketOpened != null) {
                SocketOpened(clients);
            }
        }
        protected void OnSocketClosed(List<WebSocketHandler> clients) {
            if (SocketClosed != null) {
                SocketClosed(clients);
            }
        }

    }

}